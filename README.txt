pyTLO V1.0.0 is a heuristic tapered composite laminate optimisation toolbox using Stacking Sequence Tables (SST).

--------------------------------------------------------------------------
Requirements:

1. A python IDE running python 3.5+

2. The following libraries should accessible:

	- matplotlib
	- numba (and scipy)
	- numpy

    Refer to the requirements.txt file for exact versions.

---------------------------------------------------------------------------
In order to use it:

1. clone or download the repository in your desired folder.

2. Set up your environment with the appropriate libraries.

3. Four files are present in the root folder: pyTLO.py, settings.py, structure.py and objective.py.
	- pyTLO.py is the main file you should run with your IDE of choice. Personally, I've used both
	  pyCharm and Spyder with Ananconda.

	- The other three files contain an example setup for the 18-panel Horseshoe problem.
	  They should be modified using the same layout for your own project. For more information
	  refer to the documentation provided.

4. Once you've setup your problem and/or added your own objective functions, run pyTLO.py

Refer to the documentation for more detail.
--------------------------------------------------------------------------
Folder Structure

- core and subfolders contain the files needed to run the code

- fitness_functions contains what the name implies

- project_data is where the user can dump any reference files as he sees fit

- A 'results/date_fitness_type/run_timestamp' folder will be created when the code finishes running
  where a collection of csv results files are stored 

--------------------------------------------------------------------------

Version 1.0.0

--------------------------------------------------------------------------

License

This project is licensed under the BSD 2-clause "Simplified" License. See the LICENSE.md file for details.


--------------------------------------------------------------------------
Acknowledgments

- Paul Lancelot and Marco Bordogna, my supervisors who kept demanding more and more functionality;

- Irisarri, F-X., et al. for developing the SST encoding and presenting it in:
  "Optimal design of laminated composite structures with ply drops using stacking sequence tables." 
  Composite Structures 107 (2014): 559-569.

- Anna Rusanova for having the patience to teach me the OOP basics on more than one occasion;

- The countless unsung heroes of Stack Overflow.

--------------------------------------------------------------------------

Author:

Frederico Vicente

For any questions or feedback don't hesitate to contact me at:
frederico.gsv@gmail.com
or through gitlab at https://gitlab.com/FGS_SSR/pytlo/