# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 10:18:52 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import numpy as np
from objective import Objective


class Component:
    def __init__(self, settings, component, panel_list, **kwargs):

        self.num_panel = len(panel_list)
        self.panel_list = panel_list

        self.objective = Objective(settings, panel_list, component)

        if 'neighbours' in kwargs.keys():

            self.connectivity = np.eye(self.num_panel).astype(int)

            self.neighbours = kwargs['neighbours']
            for panel in panel_list:
                idx1 = panel_list.index(panel)
                panel_neighbours = self.neighbours[panel]

                for j in panel_neighbours:

                    idx2 = panel_list.index(j)

                    self.connectivity[idx1, idx2] = 1
