# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:51:15 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import selection

from copy_ind import copy_ind

from component_crossover import component_crossover

from EvolutionOperations import EvolutionOperations

from duplicate_check import duplicate_check

import random


class Evolution:
    def __init__(self, settings, structure, CLT, evaluation, pop_creator, population):

        self.settings = settings
        self.structure = structure
        self.CLT = CLT
        self.evaluation = evaluation

        self.GA_settings = settings.GA_settings

        self.pop_creator = pop_creator

        self.pop_size = self.GA_settings.pop_size
        self.fit_weights = self.GA_settings.fit_weights
        self.cx_fraction = self.GA_settings.cx_fraction
        self.elite = self.GA_settings.elite

        self.tournament_size = 4

        self.num_components = len(self.structure.components)

        self.mutation_pct = []

        self.ga_operators = EvolutionOperations(self.settings, self.structure)

    def RunGeneration(self, population):

        '''
        SELECTION
        '''

        cx_population = population[:int(self.pop_size*self.cx_fraction)]

        kept_population = population[:int(self.pop_size*(1-self.cx_fraction))]

        offspring = []

        '''
        CROSSOVER
        '''
        total_offspring = self.pop_size - len(kept_population)

        while len(offspring) < total_offspring:

            # parent selection

            p1, p2 = selection.select_parents(cx_population, self.tournament_size,
                                              self.fit_weights)

            p1_copy, p2_copy = copy_ind(p1), copy_ind(p2)

            if self.num_components > 1:
                p1_copy, p2_copy = component_crossover(p1_copy, p2_copy)

            failed_cx = 0
            for component in self.structure.components:

                c1, c2 = p1_copy.components[component], p2_copy.components[component]
                child1, child2 = self.ga_operators.Crossover(c1, c2)

                if not child1:
                    failed_cx += 1

                else:
                    p1_copy.components[component], p2_copy.components[component] = child1, child2

            if failed_cx > int(self.num_components/2):
                continue

            if self.GA_settings.delete_duplicates is False:

                    offspring += [p1_copy]

                    offspring += [p2_copy]

            else:
                if duplicate_check(p1_copy, kept_population) is False:
                    offspring += [p1_copy]

                if duplicate_check(p2_copy, kept_population) is False:
                    offspring += [p2_copy]

        new_generation = kept_population[:] + offspring[:]

        population = new_generation[:]

        '''
        MUTATION
        '''

        total_mutations = 0

        for i in range(self.elite, self.pop_size):
            mut_attempt = False

            if random.random() < self.GA_settings.mut_pb:

                mut_attempt = True
                mutated = []

                for component_id in population[i].components:
                    component = population[i].components[component_id]

                    component = self.ga_operators.mutate(component)

                    population[i].components[component_id] = component

                    mutated += [self.ga_operators.mutated]

            population[i].Fitness = []

            if mut_attempt:
                if True in mutated:
                    total_mutations += 1 # counts 1 mutation per individual, not component

        self.mutation_pct = [int(100*(total_mutations/self.pop_size))]

        '''
        Elite Mutation - copies and mutates best individual
        Finally, replaces last individual in the population list
        '''

        mutated_elite = copy_ind(population[0])
        for component_id in mutated_elite.components:
            component = mutated_elite.components[component_id]
            component = self.ga_operators.mutate(component)
            mutated_elite.components[component_id] = component

        population[-1] = mutated_elite
        population[-1].Fitness = []

        '''
        FITNESS EVALUATION
        '''

        for individual in population:

            if not individual.Fitness:

                individual.Fitness = self.evaluation.EvaluateIndividual(individual)

        population = selection.select_best(population,
                                           self.fit_weights,
                                           self.pop_size)

        return population
