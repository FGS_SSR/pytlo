# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 18:54:08 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import random

from Nstr_crossover import Nstr_crossover

from SSTlam_crossover import SSTlam_crossover

from copy_component import copy_component

from mutation_algorithms import mutation_algorithms

# crossover and mutation functions


class EvolutionOperations:
    def __init__(self, settings, structure):

        self.structure = structure
        self.rules = settings.design_rules
        self.GA_settings = settings.GA_settings

        self.mutated = False

    def Crossover(self, parent1, parent2):

        if self.rules.enforce_thk is False:

            gene_opt = ['SSTlam', 'Nstr']

        else:

            gene_opt = ['SSTlam']

        gene_cross = random.choice(gene_opt)

        if gene_cross == 'Nstr':

            child1, child2 = Nstr_crossover(parent1, parent2, self.rules,
                                            self.structure)

        else:

            child1, child2 = SSTlam_crossover(parent1, parent2, self.rules,
                                              self.GA_settings)

        return child1, child2

    def mutate(self, component):

        mutant = copy_component(component)
        mut_count = 0
        self.mutated = False

        if self.rules.enforce_thk is False:

            mut_opt = ['Nstr', 'SSTins', 'SSTlam', 'Stack_Seq']

        else:

            mut_opt = ['SSTins', 'SSTlam', 'Stack_Seq']

        num_mut = 1

        choice_mut = random.sample(mut_opt, num_mut)

        for mut_type in choice_mut:

            if mut_type == 'Nstr':

                structure_settings = self.structure.components[component.ID]
                mut_count += 1
                mutant.Nstr = mutation_algorithms.mutate_Nstr(structure_settings,
                                                              self.rules,
                                                              component)

            if mut_type == 'SSTins':
                mut_count += 1
                mutant = mutation_algorithms.mutate_SSTins(mutant, self.rules)

            if mut_type == 'SSTlam':
                mut_count += 1
                mutant = mutation_algorithms.mutate_SSTlam(component,
                                                       self.rules,
                                                       self.GA_settings)

            if mut_type == 'Stack_Seq':
                mut_count += 1
                mutant = mutation_algorithms.stack_seq_mut(component, self.rules)

        if mut_count != 0:
            self.mutated = True

        return mutant