# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 12:25:41 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: N_str crossover operation, based on two point crossover

"""
import random

import numpy as np

from ply_drop_check import check_Nstr_cx

from gene_repair import repair_nstr

from copy_component import copy_component


def Nstr_crossover(parent1, parent2, rules, structure):

    validity = False

    size = len(parent1.Nstr)

    attempt = 0

    while validity is False:

        attempt += 1

        child1_Nstr = np.copy(parent1.Nstr).tolist()

        child2_Nstr = np.copy(parent2.Nstr).tolist()

        cutoff1 = random.randint(0, size)

        cutoff2 = random.randint(1, size-1)

        if cutoff2 >= cutoff1:
            cutoff2 += 1

        else:

            cutoff1, cutoff2 = cutoff2, cutoff1

        child1_Nstr[cutoff1:cutoff2], child2_Nstr[cutoff1:cutoff2] \
            = np.copy(parent2.Nstr[cutoff1:cutoff2]), np.copy(parent1.Nstr[cutoff1:cutoff2])

        # Nstr repair, in case an SST is attributed to an individual which does not contain said stacking sequence

        if rules.balance is True:

            child1_Nstr = repair_nstr(child1_Nstr, rules, parent1, structure)
            child2_Nstr = repair_nstr(child2_Nstr, rules, parent2, structure)

        Nstr1_valid = check_Nstr_cx(child1_Nstr, rules, structure.components[parent1.ID])
        Nstr2_valid = check_Nstr_cx(child2_Nstr, rules, structure.components[parent2.ID])

        if Nstr1_valid and Nstr2_valid:  # (True, True) condition
            child1 = copy_component(parent1)
            child2 = copy_component(parent2)

            validity = True

            child1.Nstr = np.copy(child1_Nstr)

            child2.Nstr = np.copy(child2_Nstr)

        else:

            if attempt > 30:

                child1, child2 = [], []

                break

    return child1, child2
