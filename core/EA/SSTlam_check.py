# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 16:38:15 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

from laminate_check import StackingSequenceValidation

from decoder import decoder


class Validation:

    def SSTlam_CX(individual, rules):

        SST = decoder(individual)

        for i in SST.keys():

            valid_table = StackingSequenceValidation(list(SST[i]), rules, i).valid_laminate

            if valid_table is False:
                break

        if valid_table is True:

            SST_valid = True

        else:

            SST_valid = False

        return SST_valid
