# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 14:08:52 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: crossover operation for SST_lam genes. 

"""
import random

from collections import Counter

import numpy as np

from Validation import Validation

from copy_component import copy_component

from balance_cx import balance_cx


def SSTlam_crossover(parent1, parent2, rules, GA_settings):

    ind1_lam = np.copy(parent1.SSTlam)
    ind2_lam = np.copy(parent2.SSTlam)

    if rules.balance is True:

        child1, child2 = balance_cx(parent1, parent2, rules, GA_settings)

    else:

        size = len(ind1_lam)

        validity = False

        attempt = 0

        while validity is False:

            child1 = copy_component(parent1)
            child2 = copy_component(parent2)

            ind1x_lam = np.copy(ind1_lam)
            ind2x_lam = np.copy(ind2_lam)

            cxpoint1 = random.randint(rules.cover_thk,size-1)

            cxpoint2 = random.randint(rules.cover_thk,size-1)

            if cxpoint2 >= cxpoint1:
                cxpoint2 += 1

            else: 

                cxpoint1, cxpoint2 = cxpoint2, cxpoint1

            ind1x_lam[cxpoint1:cxpoint2], ind2x_lam[cxpoint1:cxpoint2] =\
            np.copy(ind2_lam[cxpoint1:cxpoint2]), np.copy(ind1_lam[cxpoint1:cxpoint2])

            child1.SSTlam = np.copy(ind1x_lam)
            child2.SSTlam = np.copy(ind2x_lam)

            validity_child1 = Validation.SSTlam_CX(child1, rules)
            validity_child2 = Validation.SSTlam_CX(child2, rules)

            attempt += 1

            if validity_child1 and validity_child2:

                validity = True

            else:

                if attempt > 30:

                    child1, child2 = [], []
                    break

    return child1, child2
