# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 11:42:16 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Generates list of SB cycle positions and 0,90 ply orientation locations
for balanced crossover operations

"""

from collections import defaultdict


def list_balanced_pairs(ind1_ins, ind1_lam, ind2_ins, ind2_lam):

    ins_len1 = len(ind1_ins)

    ins_len2 = len(ind2_ins)

    ins_items = [list(set(ind1_ins)), list(set(ind2_ins))]

    idx_ins1 = defaultdict(list)

    idx_ins2 = defaultdict(list)

    # creates a list of indexes belonging to specific ranks of insertion
    for i in ins_items[0]:

        for j in range(ins_len1):

            if ind1_ins[j] == i:

                idx_ins1[i].append(j)

    for i in ins_items[1]:

        for j in range(ins_len2):

            if ind2_ins[j] == i:

                idx_ins2[i].append(j)

    angle_pos_ind1 = defaultdict(list)

    angle_pos_ind2 = defaultdict(list)

    # ind1 positions

    for i in idx_ins1.keys():
        checked = []

        if i == 0:  # guide laminate positions
            for j in idx_ins1[0]:

                if ind1_lam[j] not in {0, 90} and j not in checked:

                    for k in idx_ins1[i]:

                        if k not in checked and ind1_lam[k] == -ind1_lam[j]:

                            angle_pos_ind1['SB'].append([j, k])

                            checked += [j, k]
                if ind1_lam[j] in {0, 90}:

                    angle_pos_ind1['090'].append(j)

        if len(idx_ins1[i]) == 2:

            angle_pos_ind1['SB'].append(idx_ins1[i])

        elif len(idx_ins1[i]) == 1:

            if ind1_lam[idx_ins1[i][0]] == 0:

                angle_pos_ind1[0].append(idx_ins1[i][0])

            else:

                angle_pos_ind1[90].append(idx_ins1[i][0])

    # ind2 positions

    for i in idx_ins2.keys():
        checked = []

        if i == 0:
            for j in idx_ins2[0]:

                if ind2_lam[j] not in {0, 90} and j not in checked:

                    for k in idx_ins2[i]:

                        if k not in checked and ind2_lam[k] == -ind2_lam[j]:

                            angle_pos_ind2['SB'].append([j, k])

                            checked += [j, k]
                if ind2_lam[j] in {0, 90}:

                    angle_pos_ind2['090'].append(j)

        if len(idx_ins2[i]) == 2:

            angle_pos_ind2['SB'].append(idx_ins2[i])

        elif len(idx_ins2[i]) == 1:

            if ind2_lam[idx_ins2[i][0]] == 0:

                angle_pos_ind2[0].append(idx_ins2[i][0])

            else:

                angle_pos_ind2[90].append(idx_ins2[i][0])

    return angle_pos_ind1, angle_pos_ind2


def ind_positions(ind_ins, ind_lam):

    ins_len = len(ind_ins)

    ins_items = [list(set(ind_ins))]

    idx_ins = defaultdict(list)

    for i in ins_items[0]:

        for j in range(ins_len):

            if ind_ins[j] == i:

                idx_ins[i].append(j)

    angle_pos = defaultdict(list)

    # positions

    for i in idx_ins.keys():
        checked = []

        if i == 0:
            for j in idx_ins[0]:

                if ind_lam[j] not in {0, 90} and j not in checked:

                    for k in idx_ins[i]:

                        if k not in checked and ind_lam[k] == -ind_lam[j]:

                            angle_pos['SB'].append([j, k])

                            checked += [j, k]
                if ind_lam[j] in {0, 90}:

                    angle_pos['090'].append(j)

        if len(idx_ins[i]) == 2:

            angle_pos['SB'].append(idx_ins[i])

        elif len(idx_ins[i]) == 1:

            if ind_lam[idx_ins[i][0]] == 0:

                angle_pos[0].append(idx_ins[i][0])

            else:

                angle_pos[90].append(idx_ins[i][0])

    return angle_pos
