# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 11:32:19 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""

import random

from SSTlam_positions import list_balanced_pairs

from misorientation_check import check_misorientation

from Validation import Validation

from copy_component import copy_component

import numpy as np

# crossover SST_LAM balanced laminates


def balance_cx(parent1, parent2, rules, GA_options):

    ind1_SSTins = np.copy(parent1.SSTins)
    ind2_SSTins = np.copy(parent2.SSTins)

    ind1_lam    = np.copy(parent1.SSTlam)
    ind2_lam    = np.copy(parent2.SSTlam)

    angle_pos1, angle_pos2 = list_balanced_pairs(ind1_SSTins, ind1_lam, ind2_SSTins, ind2_lam)

#    SB1_count = len(angle_pos1['SB'])
#    SB2_count = len(angle_pos2['SB'])

    '''
    SB crossover option = exchanges a +-theta set between individuals, inclusive 0,90

    090 option = exchanges 0 and 90 between individuals
    '''

    valid_cx = False

    attempt_num = 0

    while not valid_cx:

        attempt_num += 1

        if attempt_num > 20:

            child1, child2 = [], []

            break

        child1 = copy_component(parent1)
        child2 = copy_component(parent2)

        lam1_cx = np.copy(ind1_lam)
        lam2_cx = np.copy(ind2_lam)

        if len(angle_pos1['090']) != 0 and len(angle_pos2['090']) != 0:

            exchange_pos1 = []
            exchange_pos2 = []

            exchange_pos1 = random.choice(angle_pos1['SB'])[:]

            exchange_pos1.append(random.choice(angle_pos1['090']))

            exchange_pos2 = random.choice(angle_pos2['SB'])[:]

            exchange_pos2.append(random.choice(angle_pos2['090']))

        else:

            exchange_pos1 = []
            exchange_pos2 = []

            exchange_pos1 = random.choice(angle_pos1['SB'])

            exchange_pos2 = random.choice(angle_pos2['SB'])

        for i in range(len(exchange_pos1)):

            lam1_cx[exchange_pos1[i]], lam2_cx[exchange_pos2[i]] = \
            np.copy(lam2_cx[exchange_pos2[i]]), np.copy(lam1_cx[exchange_pos1[i]])

        valid_mo1 = check_misorientation(lam1_cx, exchange_pos1, rules.misorientation)
        valid_mo2 = check_misorientation(lam2_cx, exchange_pos2, rules.misorientation)

        if valid_mo1 and valid_mo2:

            child1.SSTlam = np.copy(lam1_cx)
            child2.SSTlam = np.copy(lam2_cx)

            valid_cx1 = Validation.SSTlam_CX(child1, rules)

            valid_cx2 = Validation.SSTlam_CX(child2, rules)

            if valid_cx1 and valid_cx2:

                valid_cx = True

            else:

                valid_cx = False
        else:

            valid_cx = False

    return child1, child2
