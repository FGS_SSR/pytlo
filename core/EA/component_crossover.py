"""
Created on 12/12/2018 at 14:05

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Crossover function to exchange component designs between individuals
"""
import random
from core.EA.copy_component import copy_component


def component_crossover(individual1, individual2):

    list_components = list(individual1.components.keys())

    component_exchange = random.choice(list_components)

    # copy components

    c1_copy = copy_component(individual1.components[component_exchange])
    c2_copy = copy_component(individual2.components[component_exchange])

    individual1.components[component_exchange] = c2_copy
    individual2.components[component_exchange] = c1_copy

    return individual1, individual2
