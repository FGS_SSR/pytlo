# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 14:49:53 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Used to copy components and avoid using deepcopy which is
resource heavy

"""
import numpy as np


def copy_component(component):

    class ComponentCopy:
        def __init__(self, SSTlam, SSTins, Nstr, SSTkeys, ID):

            self.SSTlam = SSTlam

            self.SSTins = SSTins

            self.Nstr = Nstr

            self.SSTkeys = SSTkeys

            self.Fitness = []

            self.ID = ID

            class MechProperties:
                def __init__(self):
                    pass

            self.MechProperties = MechProperties()

    SSTlam = np.copy(component.SSTlam)
    SSTins = np.copy(component.SSTins)
    Nstr = np.copy(component.Nstr)
    SSTkeys = np.copy(component.SSTkeys)
    ID = component.ID

    component_copy = ComponentCopy(SSTlam, SSTins, Nstr, SSTkeys, ID)

    return component_copy
