# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 19:22:08 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Used to copy individuals and avoid using deepcopy which is
resource heavy
"""

import numpy as np
from collections import defaultdict


def copy_ind(ind):

    class Individual:
        def __init__(self, ind):

            self.Fitness = 0
            self.components = defaultdict(list)

            class ComponentCopy:

                def __init__(self, SSTlam, SSTins, Nstr, SSTkeys, ID):

                    self.SSTlam = SSTlam

                    self.SSTins = SSTins

                    self.Nstr = Nstr

                    self.SSTkeys = SSTkeys

                    self.Fitness = []

                    self.ID = ID

                    class MechProperties:
                        def __init__(self):
                            pass

                    self.MechProperties = MechProperties()

            for component in list(ind.components.keys()):
                SSTlam = np.copy(ind.components[component].SSTlam)
                SSTins = np.copy(ind.components[component].SSTins)
                Nstr = np.copy(ind.components[component].Nstr)
                SSTkeys = np.copy(ind.components[component].SSTkeys)
                ID = component

                self.components[component] = ComponentCopy(SSTlam, SSTins, Nstr, SSTkeys, ID)

    individual_copy = Individual(ind)

    return individual_copy
