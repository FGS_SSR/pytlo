# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 16:23:51 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Nstr crossover repair

"""
import numpy as np


def repair_nstr(Nstr, rules, ind, structure):

    size = len(Nstr)

    component_id = ind.ID

    repaired_Nstr = np.copy(Nstr)

    structure_settings = structure.components[component_id]

    for i in range(size):

        if Nstr[i] not in ind.SSTkeys:

            idx1 = structure_settings.panel_list[i]

            valid_thk = [thk for thk in ind.SSTkeys if Nstr[i] - 2 <= thk]

            for neighbours in structure_settings.neighbours[idx1]:

                idx2 = structure_settings.panel_list.index(neighbours)

                # Filter valid thickness according to max ply drop
                valid_thk = [thk for thk in valid_thk if
                             abs(thk - Nstr[idx2]) <= rules.ply_drop]

            if len(valid_thk) == 0:

                for neighbours in structure_settings.neighbours[idx1]:

                    idx2 = structure_settings.panel_list.index(neighbours)

                    valid_thk = [thk for thk in ind.SSTkeys if
                                 abs(thk - Nstr[idx2]) <= rules.ply_drop]

                repaired_Nstr[i] = min(valid_thk)

            else:

                repaired_Nstr[i] = min(valid_thk)

    return repaired_Nstr
