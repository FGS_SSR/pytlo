# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 11:16:05 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

import random
import numpy as np

from Validation import Validation

from copy_component import copy_component

from SSTlam_positions import ind_positions

from filter_angle import one_bound, two_bound

from misorientation_check import check_misorientation


class mutation_algorithms:

    def mutate_Nstr(structure, rules, individual):

        ins_set = np.copy(individual.SSTkeys).tolist()

        Nstr = np.copy(individual.Nstr)

        valid_mut = False

        num_panel = structure.num_panel

        while not valid_mut:

            mut_position = random.randint(0, (len(Nstr)-1))

            for i in range(num_panel):

                options = ins_set

                if structure.connectivity[mut_position, i] == 1 and i != mut_position:

                    options = list(filter(lambda x: abs(x - Nstr[i]) <= rules.ply_drop
                                          , options))

            if len(options) == 0:

                valid_mut = False

                continue

            else:

                Nstr[mut_position] = random.choice(options)

                valid_mut = True

        return Nstr

    def mutate_SSTins(component, rules):

        SST_ins = np.copy(component.SSTins)

        SST_lam = np.copy(component.SSTlam)

        mutant = copy_component(component)

        attempt = 0

        if rules.balance is False:

            valid_mut = False

            while not valid_mut:

                attempt += 1

                if attempt > 30:

                    mutant.SSTins = np.copy(component.SSTins)

                    break

                SST_ins_mut = np.array(SST_ins[:])

                nz_idx = np.nonzero(SST_ins[:])

                SST_ins_mut[nz_idx] = list(np.random.permutation(SST_ins[nz_idx]))

                mutant.SSTins = np.copy(SST_ins_mut)

                valid_mut = Validation.SSTlam_CX(mutant, rules)

        else:

            valid_mut = False

            idx_mutant = ind_positions(SST_ins, SST_lam)

            mut_opt = list(idx_mutant.keys())

            while not valid_mut:

                attempt += 1

                if attempt > 30:

                    mutant.SSTins = np.copy(component.SSTins)

                    break

                permute_1 = []

                permute_2 = []

                SST_ins_mut = np.copy(SST_ins)

                pick_mut = random.choice(mut_opt)

                if pick_mut in {'SB', '090'}:

                    permute_1 += random.choice(idx_mutant['SB'])

                    if '090' in mut_opt and len(idx_mutant['090']) > 1 and SST_ins[permute_1[0]] != 0:

                        if random.random() > 0.5:

                            permute_2 += list(np.random.choice(idx_mutant['090'], 2, replace = False))

                        else:

                            permute_2 += random.choice(idx_mutant['SB'])

                    else:

                            permute_2 += random.choice(idx_mutant['SB'])

                    if SST_ins[permute_1[0]] == SST_ins[permute_2[0]]:

                        valid_mut = False

                        continue

                if pick_mut in {0, 90}:

                    if idx_mutant[0] and idx_mutant[90]:

                        permute_1 += [random.choice(idx_mutant[0])]

                        permute_2 += [random.choice(idx_mutant[90])]

                    else:

                        valid_mut = False

                        continue
                if 0 in permute_1 + permute_2:

                    valid_mut = False

                    continue

                for i in range(len(permute_1)):

                    SST_ins_mut[permute_1[i]], SST_ins_mut[permute_2[i]] = \
                    SST_ins_mut[permute_2[i]].copy(), SST_ins_mut[permute_1[i]].copy()

                mutant.SSTins = SST_ins_mut.copy()

                valid_mut = Validation.SSTlam_CX(mutant, rules)

        return mutant

    def mutate_SSTlam(component, rules, GA_settings):

        validity = False

        SST_lam = np.copy(component.SSTlam)
        SST_ins = np.copy(component.SSTins)
        SSTlam_length = len(SST_lam)
        # positions of SB cycles and 0,90 plies for balanced mutations

        idx_mutant = ind_positions(SST_ins, SST_lam)

        attempt = 0

        while validity is False:

            mutant = copy_component(component)

            attempt += 1

            if attempt > 50:

                break

            possible_angles = []

            SST_lam = np.copy(mutant.SSTlam)

            angle_opt = rules.allowable_angles[:]

            if rules.balance is False:

                num_mut = np.random.randint(1, int(SSTlam_length/3))
#                num_mut = 1
                idx_mut = random.sample(range(SSTlam_length), num_mut)

                for pos in idx_mut:

                    if pos not in {0, len(SST_lam)-1}:

                        angle_mut = two_bound(rules.misorientation, angle_opt, SST_lam, pos)

                    else:

                        angle_mut = one_bound(rules.misorientation, angle_opt, SST_lam, pos)

                    if angle_mut in {1000, SST_lam[pos]}:
                        continue

                    else:

                        SST_lam[pos] = angle_mut

                mutant.SSTlam = np.copy(SST_lam)

                validity = Validation.SSTlam_CX(mutant, rules)

            else:

                angle_set = angle_opt[:]
                UD_set = [[0,0],[0,90],[90,0],[90,90]]

                mut_opt = list(idx_mutant.keys())

                picked_mut = random.choice(mut_opt)

                if picked_mut == 'SB':

                    idx_mut = random.choice(idx_mutant['SB'])

                    if rules.damtol == True and 0 in idx_mut:

                        possible_angles = [[45, -45],[-45, 45]]

                    else:

                        for i in angle_set:

                            if i == SST_lam[idx_mut[0]]:

                                continue

                            elif i not in {0, 90}:

                                SST_lam = np.copy(mutant.SSTlam)
                                SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = i, -i

                                angle_diff = check_misorientation(SST_lam, idx_mut, rules.misorientation)

                                if angle_diff is True:

                                    possible_angles += [[i, -i]]

                        for i in UD_set:

                            SST_lam = np.copy(mutant.SSTlam)
                            SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = i[0], i[1]

                            angle_diff = check_misorientation(SST_lam, idx_mut, rules.misorientation)

                            if angle_diff is True:

                                possible_angles += [i]

                    if len(possible_angles) == 0:

                        validity = False

                    else:

                        SST_lam = np.copy(mutant.SSTlam)

                        angle_mut = random.choice(possible_angles)

                        SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = angle_mut[0], angle_mut[1]

                        mutant.SSTlam = np.copy(SST_lam)

                        validity = Validation.SSTlam_CX(mutant, rules)

                if picked_mut == '090':

                    if len(idx_mutant['090']) > 1:

                        idx_mut = list(np.random.choice(idx_mutant['090'], 2))

                        for i in angle_set:

                            SST_lam = np.copy(mutant.SSTlam)
                            SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = i, -i

                            angle_diff = check_misorientation(SST_lam, idx_mut, rules.misorientation)

                            if angle_diff is True:

                                possible_angles += [[i, -i]]

                        for i in UD_set:

                            SST_lam = np.copy(mutant.SSTlam)

                            if i == [SST_lam[idx_mut[0]], SST_lam[idx_mut[1]]]:
                                continue

                            else:

                                SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = i[0], i[1]

                                angle_diff = check_misorientation(SST_lam, idx_mut, rules.misorientation)

                                if angle_diff is True:

                                    possible_angles += [i]

                    if len(possible_angles) == 0:

                        validity = False

                    else:

                        SST_lam = np.copy(mutant.SSTlam)

                        angle_mut = random.choice(possible_angles)

                        SST_lam[idx_mut[0]], SST_lam[idx_mut[1]] = angle_mut[0], angle_mut[1]

                        mutant.SSTlam = np.copy(SST_lam)

                        validity = Validation.SSTlam_CX(mutant, rules)

                if picked_mut == 0:

                    idx_mut = random.choice(idx_mutant[0])

                    SST_lam[idx_mut] = 90

                    mutant.SSTlam = np.copy(SST_lam)

                    validity = Validation.SSTlam_CX(mutant, rules)

                if picked_mut == 90:

                    idx_mut = random.choice(idx_mutant[90])

                    SST_lam[idx_mut] = 0

                    mutant.SSTlam = np.copy(SST_lam)

                    validity = Validation.SSTlam_CX(mutant, rules)

        if validity is True:

            return mutant

        else:

            return component

    def stack_seq_mut(component, rules):

        valid_mut = False

        N_ply = len(component.SSTins)

        attempt = 0

        while valid_mut is False:

            mutant = copy_component(component)

            attempt += 1

            if attempt > 30:

                break

            SST_ins = np.copy(component.SSTins)

            SST_lam = np.copy(component.SSTlam)

            if rules.covering not in {'outer', 'Outer'}:
                pos1 = random.randint(0, N_ply - 1 - rules.cover_thk)
                pos2 = random.randint(0, N_ply-1 - rules.cover_thk)

            else:
                pos1 = random.randint(rules.cover_thk, N_ply-1)
                pos2 = random.randint(rules.cover_thk, N_ply-1)

            SST_ins[pos1], SST_ins[pos2] = SST_ins[pos2], SST_ins[pos1]
            SST_lam[pos1], SST_lam[pos2] = SST_lam[pos2], SST_lam[pos1]

            mis_check = check_misorientation(SST_lam, [pos1,pos2], rules.misorientation)

            if mis_check:

                mutant.SSTins = np.copy(SST_ins)

                mutant.SSTlam = np.copy(SST_lam)

                valid_mut = Validation.SSTlam_CX(mutant, rules)

            else:

                valid_mut = False

        if valid_mut is True:

            return mutant

        else:

            return component
