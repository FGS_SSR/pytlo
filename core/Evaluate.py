# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 16:06:33 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: method evaluating individuals according to desired fitness.
If fitness function requires input other than component and component_id, line 49
should be modified accordingly.

"""
from fitness_functions.Fitness import Fitness
from core.properties.CLT_calc import lamCLT
from core.properties.LP_error_component import LP_error_component


class Evaluate:
    def __init__(self, settings, structure, CLT):

        self.settings = settings
        self.structure = structure
        self.CLT = CLT
        self.Fitness = Fitness(settings, structure, CLT)
        self.fitness_type = settings.general.fitness_type

    def EvaluateIndividual(self, individual):

        fitness = 0
        for component_id in individual.components:

            used_LP = self.structure.components[component_id].objective.relevant_LP
            component = individual.components[component_id]

            laminate_properties = lamCLT(component, self.settings, self.CLT)

            component.MechProperties.LP = laminate_properties.LP

            LP_target = self.structure.components[component_id].objective.LP_target
            panel_list = self.structure.panel_list[component_id]

            if self.settings.general.calculate_ABD is True:
                component.MechProperties.A = laminate_properties.A
                component.MechProperties.B = laminate_properties.B
                component.MechProperties.D = laminate_properties.D

            component.MechProperties.LP_error = LP_error_component(component, LP_target,
                                                                   used_LP, panel_list)

            component.Fitness = getattr(self.Fitness, self.fitness_type)(component)
            fitness += component.Fitness

        return [fitness, ]

    class EvaluatePopulation:
        def __init__(self, population):

            fitness_list = [individual.Fitness[0] for individual in population]

            self.avg_fitness = round(sum(fitness_list) / len(fitness_list), 4)

            self.min_fitness = round(min(fitness_list), 4)

            self.max_fitness = round(max(fitness_list), 4)
