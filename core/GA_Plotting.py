# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 13:09:42 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Plotting of the GA progress (lowest fit, mean)

"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
import math


class Plots:
    def __init__(self, settings, statistics):

        self.settings = settings
        self.refresh_rate = settings.GA_settings.update_cycle
        self.statistics = statistics
        self.gen_num = settings.GA_settings.max_gen

        self.label_size = 15

        self.gen_axis = np.arange(0, self.gen_num + self.refresh_rate, self.refresh_rate)

        self.color = ['r','g','b','k']

        self.marker = ['.','+','x','D','h']

    def plot_fitness(self):

        avg_fit = self.statistics.mean
        best_fit = self.statistics.best_fit

        fig1,ax1 = plt.subplots(figsize=(13,7))

        ax1.set_xlim([0, self.gen_num])
        ax1.set_ylim([0, max(avg_fit)])

        ax1.xaxis.set_major_locator(ticker
                                    .MultipleLocator(math.ceil(self.gen_num/20)))
        ax1.xaxis.set_minor_locator(ticker
                                    .MultipleLocator(math.ceil(self.gen_num/100)))

        plt.grid(True)

        plt.title(r'Average and Best fitness progression', fontsize=self.label_size)
        plt.xlabel('Generation number', fontsize=self.label_size)
        plt.ylabel('Fitness [-]', fontsize=self.label_size)

        best_fit = ax1.scatter(self.gen_axis, best_fit,
                               c=self.color[0], marker=self.marker[0],
                               linewidth=2)

        avg_fit = ax1.scatter(self.gen_axis, avg_fit,
                              c=self.color[1], marker=self.marker[1],
                              linewidth=2)

        ax1.legend(['Best Fitness', 'Average Fitness'],
                   loc='upper right', fontsize=15)

        plt.show()

        plt.savefig('fitness.png')

    def plot_mutation_stats(self):

        mut_rate = self.statistics.mutation_pct

        fig1,ax1 = plt.subplots(figsize=(13,7))

        ax1.set_xlim([0, self.gen_num])
        ax1.set_ylim([0, 100])

        ax1.xaxis.set_major_locator(ticker
                                    .MultipleLocator(math.ceil(self.gen_num/20)))
        ax1.xaxis.set_minor_locator(ticker
                                    .MultipleLocator(math.ceil(self.gen_num/100)))

        plt.grid(True)

        plt.title(r'Rate of population mutation', fontsize=self.label_size)
        plt.xlabel('Generation number', fontsize=self.label_size)
        plt.ylabel(r'Mutated Population [$\%$]', fontsize=self.label_size)

        mut_rate = ax1.scatter(self.gen_axis, mut_rate,
                               c=self.color[2], marker=self.marker[0],
                               linewidth=2)

        plt.axhline(y=self.settings.GA_settings.mut_pb*100, c=self.color[0], ls='--')

        ax1.legend(['Mutation Probability', r'$\%$ Mutated Population'],
                   loc='upper right', fontsize=15)

        plt.show()

        plt.savefig('mut_rate.png')
