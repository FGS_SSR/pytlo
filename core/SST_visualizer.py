# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 14:16:27 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""

import numpy as np

# decoder - retrieves stacking sequence from SSTlam, SSTins, Nstr

def build_stacking_sequence(individual):

    N_str = list(individual.Nstr)
    SST_ins = individual.SSTins
    SST_lam = individual.SSTlam


    ind_SST = np.ones((len(SST_lam), len(N_str)))*999

    sorted_Nstr = N_str[:]

    sorted_Nstr.sort(reverse = True)

    for i in range(len(SST_lam)):

        if SST_ins[i] == 0:

            ind_SST[i] = SST_lam[i]

        else:

            for j in range(len(N_str)):

                if SST_ins[i] <= sorted_Nstr[j]:

                    ind_SST[i,j] = SST_lam[i]

    return ind_SST