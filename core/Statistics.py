# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 16:05:41 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Stores evolution related statistics such as mean fitnesses

"""


class Statistics:
    def __init__(self):

        self.mean = []
        self.gen_count = []
        self.best_fit = []
        self.worst_fit = []
        self.mutation_pct = []
        self.HallOfFame = []

    def Store(self, gen_performance, mutation_pct, best_individual, gen_count):

        self.mean += [gen_performance.avg_fitness]
        self.best_fit += [gen_performance.min_fitness]
        self.worst_fit += [gen_performance.max_fitness]
        self.mutation_pct += mutation_pct
        self.HallOfFame += [best_individual]
        self.gen_count += [gen_count]
