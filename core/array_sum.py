# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 11:04:10 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: sum array by rows using numba

"""

import numpy as np
from numba import jit


@jit(nopython=True)
def array_sum(array):

    n_row = len(array[:, 0])

    sum_list = np.zeros((n_row, 1))

    for i in range(n_row):

        sum_list[i] = np.sum(array[i, :])

    return sum_list
