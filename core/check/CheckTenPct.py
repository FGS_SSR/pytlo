# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 13:54:38 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: 10% rule checker for guide and SST creation

"""
from collections import Counter


def CheckTenPct(stacking_sequence, rules):

    N_plies = len(stacking_sequence)

    pct_breakdown = rules.pct_breakdown

    angle_counter_pct = Counter(stacking_sequence)

    failed_pct = 0

    pct_zero = angle_counter_pct[0]/N_plies

    pct_ninety = (angle_counter_pct[90])/N_plies

    pct_nff = angle_counter_pct[-45]/N_plies

    pct_ff = angle_counter_pct[45]/N_plies

    pct_stack = [pct_zero, pct_ninety, pct_nff, pct_ff]

    for i in range(4):

        if pct_stack[i] < pct_breakdown[i]:
            failed_pct += 1

    if failed_pct != 0:
        pct_ok = False

    else:

        pct_ok = True

    return pct_ok
