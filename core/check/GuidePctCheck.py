# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 16:24:04 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: special 10% rule check for the guide

"""

from collections import Counter


def CheckGuidePct(stacking_sequence, rules):

    N_plies = len(stacking_sequence)

    pct_breakdown = rules.pct_breakdown

    angle_counter_pct = Counter(stacking_sequence)

    count = 0

    failed_pct = 0

    if int((N_plies-1)/10) != int((N_plies + 3)/10):

        extra_ply = N_plies + 3

        pct_zero = angle_counter_pct[0]/(extra_ply)

        pct_ninety = (angle_counter_pct[90])/(extra_ply)

        pct_nff = angle_counter_pct[-45]/(extra_ply)

        pct_ff = angle_counter_pct[45]/(extra_ply)

        pct_stack = [pct_zero, pct_ninety, pct_nff, pct_ff]

        while int((count + N_plies)/10) != int((N_plies + 3)/10):

            count += 1

        allowed_fail = count + 1

        for i in range(4):

            if pct_stack[i] < pct_breakdown[i]:
                failed_pct += 1

        if failed_pct > allowed_fail:

            pct_ok = False

        else:

            pct_ok = True

    else:

        pct_zero = angle_counter_pct[0]/(N_plies)

        pct_ninety = (angle_counter_pct[90])/(N_plies)

        pct_nff = angle_counter_pct[-45]/(N_plies)

        pct_ff = angle_counter_pct[45]/(N_plies)

        pct_stack = [pct_zero, pct_ninety, pct_nff, pct_ff]

        for i in range(4):

            if pct_stack[i] < pct_breakdown[i]:
                failed_pct += 1

        if failed_pct != 0:
            pct_ok = False

        else:

            pct_ok = True

    return pct_ok
