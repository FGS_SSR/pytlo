# -*- coding: utf-8 -*-
"""
Created on Tue May 29 15:31:26 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""

# Stacking sequence validity check

from collections import Counter

import numpy as np

from CheckTenPct import CheckTenPct


class StackingSequenceValidation:

    def __init__(self,stacking_sequence, rules, total_ply):

        self.broken_constraint = []

        N_plies = len(stacking_sequence)

        if rules.balance is True:

            balance_angles = list(filter(lambda x: x not in {0, 90}, stacking_sequence))

            unique_angles = list(set(balance_angles))

            angle_counter_balance = Counter(balance_angles)

            for j in unique_angles:

                if angle_counter_balance[j] != angle_counter_balance[-j]:

                    self.broken_constraint += ['balance']
                    break

        if rules.ten_pct is True:

            pct_check = CheckTenPct(stacking_sequence, rules)

            if pct_check is False:

                self.broken_constraint += ['10pct']

        if rules.ply_contiguity is True:

            for j in range(N_plies-rules.contiguity_n-1):

                contiguity_counter = Counter(stacking_sequence[j:(j + (rules.contiguity_n+1))])

                if len(contiguity_counter) == 1:

                    self.broken_constraint += ['contiguity']

                    break

            if rules.symmetry is True and np.mod(total_ply, 2) != 0:

                mid_ply_balance = Counter(stacking_sequence[-(rules.contiguity_n):])

                if len(mid_ply_balance) == 1:

                    self.broken_constraint += ['contiguity_midplane']

            if rules.symmetry is True and np.mod(total_ply, 2) == 0:

                mid_ply_balance = Counter(stacking_sequence[-(rules.contiguity_n-1):])

                if len(mid_ply_balance) == 1:

                    self.broken_constraint += ['contiguity_midplane']

        if rules.damtol is True:

            if abs(stacking_sequence[0]) != 45:

                self.broken_constraint += ['damtol']

        if rules.angle_diff is True:

            for i in range(N_plies):

                val = stacking_sequence[i]

                if i == 0:

                    compliance = abs(val-stacking_sequence[i+1]) <= rules.misorientation \
                    or abs(val-stacking_sequence[i+1]) >= (rules.misorientation + 90)

                elif i == (N_plies-1):

                    compliance = abs(val-stacking_sequence[i-1]) <= rules.misorientation \
                    or abs(val-stacking_sequence[i-1]) >= (rules.misorientation + 90)

                else:

                    ub = abs(val-stacking_sequence[i-1]) <= rules.misorientation \
                    or abs(val-stacking_sequence[i-1]) >= (rules.misorientation + 90)

                    lb = abs(val-stacking_sequence[i+1]) <= rules.misorientation \
                    or abs(val-stacking_sequence[i+1]) >= (rules.misorientation + 90)

                    if ub == lb == True:

                        compliance = True

                    else:

                        compliance = False

                if not compliance:

                    self.broken_constraint += ['misorientation']

        if len(self.broken_constraint) == 0:

            self.valid_laminate = True

        else:

            self.valid_laminate = False
