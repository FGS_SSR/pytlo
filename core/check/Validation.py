# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 16:38:15 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

from StackingSequenceValidation import StackingSequenceValidation

from decoder import decoder


class Validation:

    def SSTlam_CX(individual, rules):

        SST = decoder(individual)

        for i in SST.keys():

            valid_table = StackingSequenceValidation(list(SST[i]), rules, i).valid_laminate

            if valid_table is False:
                break

        if valid_table is True:

            SST_valid = True

        else:

            SST_valid = False

        return SST_valid

    def VerifySST(SST, ply_distribution, rules):
        SST_keys = list(SST.keys())

        for i in SST_keys:

            valid_table = StackingSequenceValidation(list(SST[i]), rules, i).valid_laminate

            if not valid_table:
                break

#        if rules.enforce_thk and rules.balance:
#            set_range = ply_distribution.set_ply_range
#
#            for range_plies in set_range:
#                valid_thk = [True for key in SST_keys if min(range_plies) <= key <= max(range_plies)]
#                if not valid_thk:
#                    valid_table = False
#                    break

        if valid_table:

            SST_valid = True

        else:

            SST_valid = False

        return SST_valid
