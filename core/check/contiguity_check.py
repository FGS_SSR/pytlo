# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 14:39:43 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np
from collections import Counter

# Contiguity check for given laminate

def check_contiguity(stacking_sequence, rules, N_ply_lim, total_ply):
    
    contiguity = True
    
    for j in range(len(stacking_sequence)-rules.contiguity_n):
            
        contiguity_counter = Counter(stacking_sequence[j:(j + (rules.contiguity_n+1))])
        
        if len(contiguity_counter) == 1:
            
            contiguity = False
            
            break

    # check contiguity at mid-plane for symmetric odd and even numbered ply counts

    if rules.symmetry == True and np.mod(total_ply,2) != 0:
        
        mid_ply_balance = Counter(stacking_sequence[-rules.contiguity_n:])
        
        if len(mid_ply_balance) == 1:
            
            contiguity = False
            
    if rules.symmetry == True and np.mod(total_ply,2) == 0:
        
        mid_ply_balance = Counter(stacking_sequence[-(rules.contiguity_n-1):])
        
        if len(mid_ply_balance) == 1:
            
            contiguity = False
    
    return contiguity