# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 21:02:07 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Checks for duplicates of given individual

"""


def duplicate_check(individual, population):

    duplicate = False

    for ind in population:
        comparison = []

        for component_id in individual.components:
            component = individual.components[component_id]

            SSTlam = list(component.SSTlam)
            SSTins = list(component.SSTins)
            Nstr = list(component.Nstr)

            comparison += [SSTlam == list(ind.components[component_id].SSTlam),
                           SSTins == list(ind.components[component_id].SSTins),
                           Nstr == list(ind.components[component_id].Nstr)]

        if False not in comparison:

            duplicate = True
            break

    return duplicate
