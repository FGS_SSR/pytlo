# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 12:23:46 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
from collections import Counter
import numpy as np

# apply 10% rule in design of stacking sequence tables

def enforce_pct(stacking_sequence, N_ply_lim, rules):

    pct_balance = []

    angle_counter_pct = Counter(stacking_sequence)

    if np.ceil(N_ply_lim.N_min/10) != np.ceil(N_ply_lim.N_max/10):

        N_ply_stack = len(stacking_sequence) + 6

    else:

        N_ply_stack = len(stacking_sequence) + 1

    pct_zero = angle_counter_pct[0]/N_ply_stack

    pct_ninety = (angle_counter_pct[90])/N_ply_stack

    pct_nff = angle_counter_pct[-45]/N_ply_stack

    pct_ff = angle_counter_pct[45]/N_ply_stack

    if pct_zero < rules.pct_zero:

        pct_balance.append(0,)

    if pct_ninety <rules.pct_ninety:

        pct_balance.append(90,)

    if pct_nff < rules.pct_nff:

        pct_balance.append(-45,)

    if pct_ff < rules.pct_ff:

        pct_balance.append(45,)

    if pct_balance:    

        return pct_balance

    else:

        return 1000
