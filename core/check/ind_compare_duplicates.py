# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 11:55:36 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Compares mutation/crossover output to existing individuals

"""


def compare_ind(ind, compare_list):

    index = []

    if not compare_list:

        duplicate = False

        return duplicate, index

    else:

        for ind_compare in compare_list:

            equal = []

            equal = [list(ind.SSTlam) == list(ind_compare[0].SSTlam),
                     list(ind.SSTins) == list(ind_compare[0].SSTins),
                     list(ind.Nstr) == list(ind_compare[0].Nstr)]

            if False not in equal:

                duplicate = True

                index = ind_compare[1]

                break

            else:

                duplicate = False

        return duplicate, index
