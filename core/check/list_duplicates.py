# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 15:39:51 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Function used to check for duplicates of individuals
in a population

"""
from ind_compare_duplicates import compare_ind


def duplicates(pop):

    duplicates_list = {}

    checked_inds = []

    i = 0

    for ind in pop:

        duplicate, index = compare_ind(ind, checked_inds)

        if duplicate is False:

            duplicates_list[i] = 0

            checked_inds += [[ind, i]]

        else:

            duplicates_list[index] += 1

        i += 1

    return duplicates_list
