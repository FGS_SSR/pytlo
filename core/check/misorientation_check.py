# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 17:52:28 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: check crossover/mutation for misorientation guidelines

inputs:
    ind = individual
    max_misorientation
"""


def check_misorientation(stacking_sequence, modified_pos, max_misorientation):

    N_plies = len(stacking_sequence)

    for i in modified_pos:

        val = stacking_sequence[i]

        if i == 0:

            compliance = abs(val-stacking_sequence[i+1]) <= max_misorientation \
            or abs(val-stacking_sequence[i+1]) >= (max_misorientation + 90)

        elif i == (N_plies-1):

            compliance = abs(val-stacking_sequence[i-1]) <= max_misorientation \
            or abs(val-stacking_sequence[i-1]) >= (max_misorientation + 90)

        else:

            ub = abs(val-stacking_sequence[i-1]) <= max_misorientation \
            or abs(val-stacking_sequence[i-1]) >= (max_misorientation + 90)

            lb = abs(val-stacking_sequence[i+1]) <= max_misorientation \
            or abs(val-stacking_sequence[i+1]) >= (max_misorientation + 90)

            if ub == lb == True:

                compliance = True

            else:

                compliance = False

    return compliance
