# -*- coding: utf-8 -*-
"""
Created on Fri Jun  8 12:10:55 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np
# Position probability to distribute ply insertion throughout laminate
# To ensure internal continuity, probability increases away from previous position

def choose_position(possible_positions, prev_position):

    avg_p = 1/(len(possible_positions)+1)

    probability_vector = [0]*len(possible_positions)

    for i in range(len(probability_vector)):

        probability_vector[i] = avg_p*(abs(prev_position-i))

    cumulative = sum(probability_vector)

    for j in range(len(probability_vector)):

        probability_vector[j] = probability_vector[j]/cumulative    # normalize sum to 1

    insert_position = np.random.choice(possible_positions, p = probability_vector)

    return insert_position
