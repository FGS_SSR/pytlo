# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 17:32:46 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

File Description:   track number of total added plies to laminate
                    during SST build operation
"""


def ply_count(type_add, rules, previous_count, stacking_sequence):

    # input stacking sequence before add operations

    if rules.symmetry is True:

        if type_add == 'SB':

            sum_plies = previous_count + 4

        if type_add == 'single':

            sum_plies = previous_count + 2

    if rules.symmetry is False:

        if type_add == 'SB':

            sum_plies = previous_count + 2

        if type_add == 'single':

            sum_plies = previous_count + 1

    return sum_plies
