# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 11:49:19 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:
checks if the ply drop rule is enforced for a given Nstr distribution 
"""


def check_Nstr_cx(Nstr, rules, structure):

    validity = True

    for i in range(len(Nstr)):

        for j in range(len(Nstr)):

            if structure.connectivity[i, j] == 1 and i != j:

                ply_drop = abs(Nstr[i] - Nstr[j])
                if ply_drop > rules.ply_drop:

                    validity = False

                    break

        if validity is False:
            break

    return validity
