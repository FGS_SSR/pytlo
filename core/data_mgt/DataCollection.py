# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 14:49:47 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

import os
import datetime

from store_component import store_component
from store_statistics import store_statistics
from store_LP import store_LP
from store_SST import store_SST
from store_structure import store_structure
from store_LP_error import store_LP_error

class DataCollection:
    def __init__(self, settings, structure, best_candidate, statistics):

        self.settings = settings
        self.best_candidate = best_candidate
        self.stats = statistics
        self.structure = structure


        if settings.data_settings.timestamp_dir:
            self.timestamp_dir = datetime.datetime.now().strftime('%Y-%m-%d')

            self.run_id = datetime.datetime.now().strftime('%Hh_%Mm_%Ss')

            self.main_dir = os.path.join('results',
                                         settings.data_settings.batch_id,
                                         self.run_id)

        else:
            self.run_id = settings.data_settings.run_id +\
            '{}'.format(settings.data_settings.GA_counter)

            self.main_dir = os.path.join('results',
                                         settings.data_settings.batch_id,
                                         self.run_id)

        self.error_dir = os.path.join(self.main_dir, 'lp_error')

        self.lp_retdir = os.path.join(self.main_dir, 'lp_retrieved')

        # create folders

        os.makedirs(self.main_dir, exist_ok=True)

    # Store best design candidate data

    def Store(self):

        store_statistics(self, file_name='evolution_statistics.csv')

        for component_id in self.best_candidate.components:

            self.dir_name = (self.main_dir + '/retrieved_design/{}'.format(component_id))

            os.makedirs(self.dir_name, exist_ok=True)

            self.component_id = component_id

            self.component = self.best_candidate.components[component_id]

            store_component(self, file_name='{}.csv'.format(self.component_id))

            store_LP(self, file_name='LP_results.csv')

            store_SST(self, file_name='retrieved_SST.csv')

            store_structure(self, file_name='patch_properties.csv')

            store_LP_error(self, file_name='LP_error.csv')
