# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 11:40:35 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Used to collect data of multiple runs such as best fitness.
"""
import os
import csv


def multi_run_collection(settings, optimiser_object):

    opt = optimiser_object

    file_name = settings.data_settings.batch_id + '_run_fitness.csv'
    dir_name = os.path.join('results', settings.data_settings.batch_id)

    run_id = opt.results.run_id

    run_fitness = [run_id, opt.best_individual.Fitness[0]]

    with open(os.path.join(dir_name, file_name), 'a', newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(run_fitness)

    outfile.close()
