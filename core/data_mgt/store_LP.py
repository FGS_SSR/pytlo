# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 17:45:15 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Writes retrieved and target LP in csv format.

"""

import numpy as np
import os
import csv

def store_LP(self, file_name):

    component = self.component
    structure = self.structure

    retrieved_LP = np.zeros((12, len(structure.panel_list[self.component_id])))
    target_LP = np.zeros((12, len(structure.panel_list[self.component_id])))

    Nstr = component.Nstr

    for panel in structure.panel_list[self.component_id]:
        idx_panel = structure.panel_list[self.component_id].index(panel)
        retrieved_LP[:,idx_panel] = component.MechProperties.LP[Nstr[idx_panel]]
        target_LP[:, idx_panel] = structure.components[self.component_id].objective.LP_target[panel]

    headers = ['Panel Number', 'V1A', 'V2A', 'V3A', 'V4A', 'V1B', 'V2B',
               'V3B', 'V4B', 'V1D', 'V2D', 'V3D', 'V4D']

    retrieved_data = zip(structure.panel_list[self.component_id], *retrieved_LP)
    target_data = zip(structure.panel_list[self.component_id], *target_LP)

    csv.field_size_limit(200000)

    with open(os.path.join(self.dir_name, file_name), 'w', newline='') as outfile:

        writer = csv.writer(outfile)

        writer.writerow(['', '', '', '', '', '', 'Retrieved LPs'])
        writer.writerow(headers)
        writer.writerows(retrieved_data)
        writer.writerow([])

        writer.writerow(['', '', '', '', '', '', 'Target LPs'])
        writer.writerow(headers)
        writer.writerows(target_data)

    outfile.close()
