# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 17:45:15 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Writes retrieved and target LP in csv format.

"""

import os
import csv
from LP_error import LP_error


def store_LP_error(self, file_name):

    component = self.component
    structure = self.structure

    retrieved_LP = component.MechProperties.LP
    target_LP = structure.components[self.component_id].objective.LP_target

    Nstr = component.Nstr

    error_data = []
    for panel in structure.panel_list[self.component_id]:
        panel_idx = structure.panel_list[self.component_id].index(panel)
        error_data += [LP_error(retrieved_LP[Nstr[panel_idx]], target_LP[panel])]

    with open(os.path.join(self.dir_name, file_name), 'w', newline='') as outfile:

        writer = csv.writer(outfile)
        writer.writerows(error_data)

    outfile.close()
