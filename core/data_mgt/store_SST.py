# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 11:58:11 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import csv
import os


def store_SST(self, file_name):

    component = self.component

    SSTins = component.SSTins
    SSTlam = component.SSTlam
    SSTkeys = component.SSTkeys[::-1]
    symmetry = self.settings.design_rules.symmetry
    N_ply = len(SSTlam)

    SST = []

    for i in range(N_ply):

        key_filter = [k for k in SSTkeys if k >= SSTins[i]]

        patch = [SSTlam[i]]*len(key_filter)

        SST += [patch]

    headers = [['Ply Count'], SSTkeys]

    with open(os.path.join(self.dir_name, file_name), 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerows(headers)
        writer.writerow([])
        writer.writerows(SST)

        if symmetry is True:
            writer.writerow(['symmetric SST'])

    outfile.close()
