# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 17:38:05 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: saves data to csv

"""
from itertools import zip_longest
import csv
import os


def store_component(self, file_name):

    component = self.component
    design_rules = self.settings.design_rules

    design_constraints = []

    # create design constraints list (hacky, I know)
    if design_rules.symmetry is True:
        design_constraints += ['Symmetry']

    if design_rules.balance is True:
        design_constraints += ['Balance']

    if design_rules.ten_pct is True:
        design_constraints += ['Ten pct']

    if design_rules.damtol is True:
        design_constraints += ['damtol']

    if design_rules.ply_contiguity is True:
        design_constraints += ['Contiguity']

    if design_rules.angle_diff is True:
        design_constraints += ['Misorientation']

    design_constraints += ['Max Ply drop: {}'.format(design_rules.ply_drop)]

    individual_data = [list(component.SSTlam), list(component.SSTins),
                       list(component.Nstr), [component.Fitness], design_constraints]

    export_data = zip_longest(*individual_data, fillvalue='')

    headers = ['Stacking Sequence','Insertion Rank','Panel Ply Count','Fitness','Design Constrains']

    with open(os.path.join(self.dir_name, file_name), 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(headers)
        writer.writerows(export_data)

    outfile.close()
