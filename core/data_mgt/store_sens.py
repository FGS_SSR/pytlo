# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 11:59:48 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Prints index of selected sensitivities to file

"""
import csv
import os
from collections import defaultdict


def store_sens(index_list, load_cases, dir_name, file_name):

    data = []
    header = []
    sub_header = []

    sens_types = list(index_list.keys())

    counter = 0

    for item in sens_types:
        header += ['']*counter +[item]
        counter += 1
        sub_header += load_cases

    for load_case in load_cases:
        for sens_type in index_list:

            data[sens_type] = [sens_type]
        for idx in index_list[sens_type]:
            data[sens_type] += [idx]

    with open(os.path.join(dir_name, file_name), 'w', newline='') as outfile:
        writer = csv.writer(outfile)

        for sens_type in data:
            writer.writerow(data[sens_type])
    outfile.close()
