# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 16:00:35 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

import csv
import os


def store_statistics(self, file_name):

    statistics = self.stats

    gen_list = statistics.gen_count
    mean_fit = statistics.mean
    best_fit = statistics.best_fit
    worst_fit = statistics.worst_fit

    stats_data = [gen_list, best_fit, mean_fit, worst_fit]

    export_data = zip(*stats_data)

    headers = ['Generation', 'Min_fit', 'Avg_fit', 'Worst_fit']

    csv.field_size_limit(200000)

    with open(os.path.join(self.main_dir, file_name), 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(headers)
        writer.writerows(export_data)

    outfile.close()
