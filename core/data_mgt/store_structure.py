# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 13:46:57 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import csv
import os
from itertools import zip_longest


def store_structure(self, file_name):

    panel_list = self.structure.panel_list[self.component_id]
    symmetry = self.settings.design_rules.symmetry
    component = self.component

    SSTins = component.SSTins
    SSTlam = component.SSTlam
    Nstr = list(component.Nstr)

    N_ply = len(SSTlam)

    laminates = []

    for i in panel_list:
        idx_panel = panel_list.index(i)

        patch = []

        for j in range(N_ply):

            if SSTins[j] <= Nstr[idx_panel]:

                patch += [SSTlam[j]]

        laminates += [patch]

    laminates = zip_longest(*laminates, fillvalue='')

    headers = [['Patch details incl. Stacking Sequence and Ply Number'], panel_list, Nstr]

    with open(os.path.join(self.dir_name, file_name), 'w', newline='') as outfile:
        writer = csv.writer(outfile)

        writer.writerows(headers)

        writer.writerow([])

        writer.writerows(laminates)

#        writer.writerow(['buckling ratios'])
#        writer.writerow(buckling_ratios)

        if symmetry is True:
            writer.writerow(['symmetric laminates'])

        writer.writerow(['total Num Plies: ', sum(Nstr)])

    outfile.close()
