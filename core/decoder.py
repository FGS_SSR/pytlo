# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:00:47 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
# recreates stacking sequence tables of an individual


def decoder(component):

    N_str = set(component.Nstr)
    SST_ins = component.SSTins
    SST_lam = component.SSTlam

    SStables = {}

    for i in N_str:

        # filters SSTlam according to SST_ins vector by removing any angles with SSTins > Nstr[i]

        SStables[i] = [k for k, j in zip(SST_lam, SST_ins) if j <= i]

    return SStables
