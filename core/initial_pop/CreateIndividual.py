# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 14:38:43 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
from collections import defaultdict
from core.initial_pop.StructuralComponent import StructuralComponent


def CreateIndividual(settings, structure):

    class Individual:
        def __init__(self, settings, structure):

            self.Fitness = 0
            self.components = defaultdict(list)

            for component in list(structure.keys()):

                self.components[component] = StructuralComponent(settings,
                                                                 structure[component],
                                                                 component)

    individual_container = Individual(settings, structure)

    return individual_container
