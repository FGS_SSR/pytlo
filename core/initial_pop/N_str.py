# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 15:28:54 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import panel_connectivity
import n_str_fun

# Nstr class where ply distribution is generated
  
def gen_N_str(N_ply_lim, SS_tables, rules, pop_size):

    connectivity = panel_connectivity.panel_structure()
    
    N_str = {}
     
    for i in range(pop_size):
        
        N_str[i] = n_str_fun.create_Nstr(connectivity.panel_connectivity, 
             list(SS_tables[i].keys()), N_ply_lim,connectivity.panel_list, rules)
        
    return N_str