# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 18:23:52 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
from core.initial_pop.CreateIndividual import CreateIndividual

from core.selection import select_best

from core.Evaluate import Evaluate

class PopGenerator:

    def __init__(self, settings, structure, CLT, evaluation):

        self.settings = settings
        self.CLT = CLT
        self.structure = structure
        self.evaluation = evaluation

    def CreatePopulation(self, initial_size, pop_size):

        initial_pop = []

        for i in range(initial_size):

            initial_pop += [CreateIndividual(self.settings, self.structure.components)]
        '''
        initial_pop[3].components['UW'].SSTlam =\
        [-45, -30, 45, 45, 45, 45, 45, 60, 60, 60,
         60, -75, -75, -75, 15, 0, 15, 0, 60, 60]
        initial_pop[3].components['UW'].SSTins =\
        [38, 40, 24, 22, 26, 0, 34, 0, 0, 0, 16,
         0, 0, 0, 32, 30, 28, 36, 20, 18]
        initial_pop[3].components['UW'].Nstr =\
        [38, 34, 24, 38, 26, 40, 24, 34, 20, 28, 18, 22, 16, 14]

        initial_pop[3].components['LW'].SSTlam =\
        [45, -45, 30, 60, -30, 15, 15, 0, 0,
         -15, -15, -15, -15, 0, -15, 0, 0, 0]
        initial_pop[3].components['LW'].SSTins =\
        [0, 0, 36, 0, 18, 32, 24, 0, 30, 22,
         0, 16, 20, 28, 0, 34, 0, 26]
        initial_pop[3].components['LW'].Nstr =\
        [32, 30, 24, 34, 28, 36, 28,
         30, 24, 26, 22, 24, 20, 14]

        initial_pop[3].components['RS'].SSTlam =\
        [15, -45, -75, 90, -30, 75, -45, 60, -45, 60]
        initial_pop[3].components['RS'].SSTins =\
        [0, 0, 20, 16, 18, 0, 12, 0, 0, 14]
        initial_pop[3].components['RS'].Nstr =\
        [20, 12, 18, 14, 12, 12, 10, 16]

        initial_pop[3].components['FS'].SSTlam =\
        [-15, -60, 45, 45, 45, 30, 30, 45, -60]
        initial_pop[3].components['FS'].SSTins =\
        [0, 0, 14, 0, 0, 12, 16, 18, 0]
        initial_pop[3].components['FS'].Nstr =\
        [12, 12, 18, 18, 16, 14, 10, 12]
        '''
        for individual in initial_pop:

            individual.Fitness = self.evaluation.EvaluateIndividual(individual)

        population = select_best(initial_pop,
                                 self.settings.GA_settings.fit_weights,
                                 pop_size)

        return population
