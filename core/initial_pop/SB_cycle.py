# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:51:51 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""

import pick_position
import numpy as np

#SB-Cycle V2, now class based!

class SB_cycle:
    def __init__(self, stacking_sequence, pick_angle, rules, prev_position, total_add):
        
        self.balance_positions = []
        
        # limits related to number of plies required for outer or inner cover
        
        if rules.covering in {'Outer', 'outer'}:
        
            cover_rule_top = rules.cover_thk
            
            cover_rule_bottom = len(stacking_sequence)+1
            
            if np.mod(total_add,2) != 0:
                
                cover_rule_bottom = len(stacking_sequence)-1
                
        else:
        
            cover_rule_top = 0
            
            cover_rule_bottom = len(stacking_sequence)+1-rules.cover_thk
            
            if np.mod(total_add,2) != 0:
                
                cover_rule_bottom = len(stacking_sequence)-rules.cover_thk
            
            
        for i in range(cover_rule_top, cover_rule_bottom):
        
            if i in {len(stacking_sequence), 0}:
            
                prev_angle = (abs(-pick_angle-stacking_sequence[i-1]) <= rules.misorientation
                or abs(-pick_angle-stacking_sequence[i-1]) >= (rules.misorientation + 90))
            
                if prev_angle is True:
                
                    self.balance_positions.append(i)
                
                
            else:
                
                prev_angle = (abs(-pick_angle-stacking_sequence[i-1]) <= rules.misorientation
                or abs(-pick_angle-stacking_sequence[i-1]) >= (rules.misorientation + 90))
                
                next_angle = (abs(-pick_angle-stacking_sequence[i]) <= rules.misorientation 
                or abs(-pick_angle-stacking_sequence[i]) >= (rules.misorientation + 90))
                
                if prev_angle == next_angle == True:
                
                    self.balance_positions.append(i)
                    
                    
        if len(self.balance_positions) == 0:
            
            self.insert_balance_position = 1000
            self.balanced_stack = None
            self.cycle_complete = False
        
        else:
            
            self.insert_balance_position = pick_position.choose_position(self.balance_positions, 
                                                                         prev_position)
    
            self.balanced_stack = np.insert(stacking_sequence,
                                       self.insert_balance_position, 
                                       -pick_angle)
            self.cycle_complete = True