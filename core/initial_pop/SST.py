# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 15:46:03 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
#libraries
import numpy as np

# files

from SST_ind import SST_ind

from Validation import Validation

from guide_creation import guide_create

# Stacking Sequence Tables Class


class StackSeqTables:

    def __init__(self, settings, structure):

        ply_distribution = structure.objective.ply_distribution
        rules = settings.design_rules
        allowable_angles = settings.design_rules.allowable_angles

        SST_success = False

        while SST_success is False:
            self.tables = {}

            thin_guide = guide_create(settings, structure)

            self.tables[ply_distribution.min_ply_count] = thin_guide[:]

            added_positions = [0, ]

            self.SST_ins = [0]*ply_distribution.N_min

            # used to determine initial N_ply
            total_ply = ply_distribution.min_ply_count
            # constant stiffness laminate condition

            if ply_distribution.min_ply_count == ply_distribution.max_ply_count:
                SST_success = True
                continue

            while total_ply < ply_distribution.max_ply_count:

                pos_orientations = allowable_angles[:]

                stacking_sequence = self.tables[total_ply]

                if total_ply == (ply_distribution.max_ply_count - 1) and rules.balance is True:

                    pos_orientations = [0, 90]

                '''
                for balanced laminates, after a ply is chosen,
                it is checked if it is possible to balance it.
                If not, cycle_complete=false and loop runs again
                until a balanced laminate is created.
                '''

                SST = SST_ind(stacking_sequence, pos_orientations, rules, structure,
                              added_positions[0], self.SST_ins, total_ply)


                if SST.success is True:

                    self.tables[SST.total_ply] = SST.balanced_stack

                else:

                    break

                total_ply = SST.total_ply

                for t in range(0, len(SST.added_positions)):

                    self.SST_ins = np.insert(self.SST_ins, SST.added_positions[t], total_ply)

            valid_SST = Validation.VerifySST(self.tables, ply_distribution, rules)

            if SST.success and valid_SST:
                SST_success = True

            else:
                continue
