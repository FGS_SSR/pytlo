# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 13:26:11 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

Variable descriptions:
    p_lim = limits range of positions a ply can be inserted in.

"""
# imported libraries
import numpy as np

# imported files
import filter_angle
import contiguity_check
import enforce_pct
import pick_position

from SB_cycle import SB_cycle
from ply_count_tracker import ply_count


class SST_ind:
    def __init__(self, stacking_sequence, allowable_orientations, rules,
                 structure, prev_position, SST_ins_ind, total_add):

        cycle_complete = False
        ply_distribution = structure.objective.ply_distribution
        ply_options = allowable_orientations[:]

        length = len(stacking_sequence)

        failed_cycles = 0

        self.success = True

        while not cycle_complete:

            # pick random position in given laminate to insert a ply

            if rules.covering in {'outer', 'Outer'}:

                p_lim = length + 1

                insert_position = pick_position.choose_position(range(rules.cover_thk, p_lim), prev_position)

            else:

                p_lim = length + 1 - rules.cover_thk

                insert_position = pick_position.choose_position(range(0, p_lim), prev_position)

            if rules.ten_pct is True:

                pct_balance = enforce_pct.enforce_pct(stacking_sequence,
                                                      ply_distribution,
                                                      rules)

                if pct_balance != 1000:

                    ply_options = pct_balance[:]

                else:

                    ply_options = list(allowable_orientations)

            if insert_position in {length, 0}:

                '''
                restrict possible ply angles in case insert position is the midply
                and determine which angle filter should be used.

                '''

                pick_angle = filter_angle.one_bound(rules.misorientation,
                                                    ply_options,
                                                    stacking_sequence,
                                                    insert_position)
            else:

                pick_angle = filter_angle.two_bound(rules.misorientation,
                                                    ply_options,
                                                    stacking_sequence,
                                                    insert_position)

            if pick_angle == 1000:   # meaning no angle can be placed at chosen position

                cycle_complete = False
                continue

            stack_add_op = np.insert(stacking_sequence,
                                     insert_position, pick_angle)

            if rules.balance is True and pick_angle not in {0, 90}:

                type_add = 'SB'

                balancing_act = SB_cycle(stack_add_op,
                                         pick_angle,
                                         rules,
                                         insert_position,
                                         total_add)

                self.balanced_stack = balancing_act.balanced_stack
                cycle_complete = balancing_act.cycle_complete

                self.SB_add = True

            else:

                type_add = 'single'

                self.balanced_stack = stack_add_op[:]

                cycle_complete = True

                self.SB_add = False

            if self.SB_add is True:

                self.added_positions = [insert_position, balancing_act.insert_balance_position]

            else:

                self.added_positions = [insert_position, ]

            if cycle_complete is True:

                self.total_ply = ply_count(type_add, rules, total_add,
                                           stacking_sequence)

                if self.total_ply > ply_distribution.max_ply_count:

                    cycle_complete = False

            if rules.ply_contiguity and cycle_complete:

                cycle_complete = contiguity_check.check_contiguity(self.balanced_stack,
                                                                   rules,
                                                                   ply_distribution,
                                                                   self.total_ply)

            failed_cycles += 1

            if failed_cycles > 100:

                self.success = False

                break
