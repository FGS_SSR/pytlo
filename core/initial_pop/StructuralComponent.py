# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 14:38:43 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
from SST import StackSeqTables

from n_str_fun import create_Nstr


class StructuralComponent:

    def __init__(self, settings, structure, component):
        self.settings = settings
        self.structure = structure

        SST = StackSeqTables(self.settings, self.structure)

        N_str = create_Nstr(self.settings, self.structure, list(SST.tables.keys()))

        self.SSTlam = SST.tables[self.structure.objective.ply_distribution.max_ply_count]

        self.SSTins = SST.SST_ins

        self.Nstr = N_str

        self.SSTkeys = list(SST.tables.keys())

        self.Fitness = []

        self.ID = component

        class MechProperties:
            def __init__(self):
                pass

        self.MechProperties = MechProperties()
