# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 10:37:23 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
from collections import Counter

# Balance guide laminates

def balance_guide(stacking_sequence, rules, insert_position):

    balance_wait_list = []
    balance_options = []
                
    balance_angles = list(filter(lambda x: x not in {0,90}, stacking_sequence))
    
    
    if len(balance_angles) == 0:
        
        balance_options = 1000
        
        return balance_options
    
    
    else:
        
        unique_angles = list(set(balance_angles))

        angle_counter = Counter(balance_angles)
        
        for j in range(len(unique_angles)):                        
            
            if angle_counter[unique_angles[j]] > angle_counter[-unique_angles[j]]:
                
                balance_wait_list.extend(list((-unique_angles[j],)))
                
                
            elif angle_counter[unique_angles[j]] < angle_counter[-unique_angles[j]]:
                
                balance_wait_list.extend(list((unique_angles[j],)))
            
            
        if len(balance_wait_list) > 3:
            
            balance_wait_list.extend([0,90])
            
            
        balance_options = list(filter(lambda x: abs(x-stacking_sequence[insert_position-1]) <= rules.misorientation 
                                  or abs(x-stacking_sequence[insert_position-1]) >= (rules.misorientation + 90),
                                  balance_wait_list))
        
        if len(balance_options) == 0:
            
            balance_options = 1000
            
        return balance_options