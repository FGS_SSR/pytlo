# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 16:27:35 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np

#angle filtering to create SS tables and thin guides

def one_bound(max_misorientation, allowable_orientations, stacking_sequence, insert_position):

    if len(stacking_sequence) != 1:

        if insert_position == 0:

            possible_angles = list(filter(lambda x: abs(x-stacking_sequence[insert_position+1]) <= max_misorientation 
                                      or abs(x-stacking_sequence[insert_position+1]) >= (max_misorientation + 90),
                                      allowable_orientations))

        else:

            possible_angles = list(filter(lambda x: abs(x-stacking_sequence[insert_position-1]) <= max_misorientation 
                                      or abs(x-stacking_sequence[insert_position-1]) >= (max_misorientation + 90),
                                      allowable_orientations))
    else:
        possible_angles = allowable_orientations[:]

    if not possible_angles:

        pick_angle = 1000

    else:

        pick_angle = np.random.choice(possible_angles)

    return pick_angle

# misorientation filtering for position bounded in both sides

def two_bound(max_misorientation, allowable_orientations, stacking_sequence, insert_position):

    possible_angles_upper = list(filter(lambda x: abs(x-stacking_sequence[insert_position-1]) <= max_misorientation 
                                      or abs(x-stacking_sequence[insert_position-1]) >= (max_misorientation + 90)
                                      , allowable_orientations))

    possible_angles_lower = list(filter(lambda x: abs(x-stacking_sequence[insert_position]) <= max_misorientation 
                                      or abs(x-stacking_sequence[insert_position]) >= (max_misorientation + 90)
                                      , allowable_orientations))

    possible_angles = [i for i in possible_angles_upper if i in possible_angles_lower]

    if not possible_angles:

        pick_angle = 1000

    else:

        pick_angle = np.random.choice(possible_angles)

    return pick_angle
