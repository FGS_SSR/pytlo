# -*- coding: utf-8 -*-
"""
Created on Wed May 30 16:12:57 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np

from StackingSequenceValidation import StackingSequenceValidation
import filter_angle
from balance_guide import balance_guide
from GuidePctCheck import CheckGuidePct


def guide_create(settings, structure):

    # Guide laminate list set up

    ply_distribution = structure.objective.ply_distribution
    rules = settings.design_rules
    allowable_orientations = settings.design_rules.allowable_angles
    ten_pct = rules.ten_pct

    stacking_sequence = [0] * ply_distribution.N_min

    valid_thin = False

    N_ply = ply_distribution.N_min

    # Filling positions w.r.t constraints. Yields valid laminate guide at the end of each loop.

    while valid_thin is False:

        for i in range(0, N_ply):

            if rules.damtol is True and i == 0:

                outer_opt = list(filter(lambda x: rules.damtol_lower_lim <=
                                        abs(x) <= rules.damtol_upper_lim,
                                        allowable_orientations))

                stacking_sequence[0] = np.random.choice(outer_opt)
                continue

            else:

                placeholder = filter_angle.one_bound(rules.misorientation,
                                                     allowable_orientations,
                                                     stacking_sequence, i)

            if rules.balance is True and i >= 1:

                stacking_sequence[i] = placeholder

                balance_options = balance_guide(stacking_sequence, rules, i)

                if balance_options == 1000:

                    stacking_sequence[i] = placeholder

                    continue

                else:
                    stacking_sequence[i] = np.random.choice(balance_options)

            else:
                stacking_sequence[i] = placeholder

        check = []

        verification = StackingSequenceValidation(stacking_sequence,
                                                  rules,
                                                  ply_distribution.min_ply_count).valid_laminate

        check += [verification]

        if ten_pct is True:
            TenPctCheck = CheckGuidePct(stacking_sequence, rules)
            check += [TenPctCheck]

        if False not in check:
            valid_thin = True

    return stacking_sequence
