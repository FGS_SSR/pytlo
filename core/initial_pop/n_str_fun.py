# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 18:21:48 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:
    
"""

import numpy as np

from bisect import bisect_left

#import warnings

import random


def create_Nstr(settings, structure, SST_keys):

    ply_distribution = structure.objective.ply_distribution

    panel_list = structure.panel_list

    rules = settings.design_rules

    options = SST_keys[:]   # lists the length of all available stacking tables for a given individual

    min_ply_dist = []

    min_ply_dist = [min(i) for i in ply_distribution.ply_distribution]

    max_ply_dist = []

    max_ply_dist = [max(i) for i in ply_distribution.ply_distribution]

    N_str = [0] * structure.num_panel

    panel_state = [0] * structure.num_panel

    '''
    Panel state keeps track of which panels have been assigned a stacking sequence,
    so that it is used in checking the ply drop limit rule when neighbouring panels are 
    being assigned their stacking sequence

    '''

    if rules.enforce_thk is True:

        for i in range(structure.num_panel):
            options = list(filter(lambda x: min_ply_dist[i] <=  x <= max_ply_dist[i], SST_keys))

            if not options:
                nearest_ply_num = bisect_left(SST_keys,
                                              min(ply_distribution.ply_distribution[i]))
                options = [SST_keys[nearest_ply_num]]

            N_str[i] = random.choice(options)

    else:

        neighbours = structure.neighbours
        for i in range(structure.num_panel):

            options = list(filter(lambda x: min_ply_dist[i] <=  x <= max_ply_dist[i], SST_keys))

            if not options:

                nearest_ply_num = bisect_left(SST_keys, min(ply_distribution.ply_distribution[i]))

                options = [SST_keys[nearest_ply_num]]

            for j in neighbours[panel_list[i]]:

                idx_panel = panel_list.index(j)

                if panel_state[idx_panel] == 1:

                    options = list(filter(lambda x: abs(x - N_str[idx_panel])
                                   <= rules.ply_drop, options))

            if len(options) == 0:

                surrounding_patch_count = []

                for j in neighbours[panel_list[i]]:

                    idx_panel = panel_list.index(j)

                    if panel_state[idx_panel] == 1:

                        surrounding_patch_count += [N_str[idx_panel]]

                options = list(filter(lambda x: max(surrounding_patch_count)-rules.ply_drop <= x
                                      <= min(surrounding_patch_count)+rules.ply_drop, SST_keys))

            N_str[i] = random.choice(options)

            panel_state[i] = 1

    return N_str