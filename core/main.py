"""
@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: main function for a single GA run

"""
from core.optimiser import Optimiser

from core.GA_Plotting import Plots


def main(settings, structure):

    opt = Optimiser(settings, structure)

    # runs optimiser to return a stacking sequence table (SST)

    opt.Run()

    statistics = opt.stats

    plots = Plots(settings, statistics)

    return opt, plots
