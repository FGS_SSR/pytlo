# -*- coding: utf-8 -*-
"""
@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Executable for stacking sequence retrieval tool

"""

from core.properties.CLT_properties import CLT_properties

from core.initial_pop.PopGenerator import PopGenerator

from core.EA.Evolution import Evolution

from core.Evaluate import Evaluate

from core.progress import progress

import core.Statistics as Statistics

import core.selection as selection

import numpy as np

import timeit

from core.data_mgt.DataCollection import DataCollection


class Optimiser:
    def __init__(self, settings, structure):

        self.settings = settings

        self.structure = structure

        self.GA_settings = self.settings.GA_settings

        self.best_individual = []

        self.stats = Statistics.Statistics()

        self.CLT = CLT_properties(settings)

        self.evaluation = Evaluate(self.settings, self.structure, self.CLT)

        '''
        1. Calculates rotated ply stiffnesses for allowable angles
        according to Classical Lamination Theory, Plane-stress assumption
        '''

    def Run(self):

        start = timeit.default_timer()

        '''
        2. Initial population generated according to user settings
        '''

        popgenerator = PopGenerator(self.settings, self.structure, self.CLT, self.evaluation)

        population = popgenerator.CreatePopulation(self.GA_settings.initial_pop,
                                                   self.GA_settings.pop_size)

        print('Population Created.')

        '''
        3. Genetic Algorithm
        '''
        gen = 1

        evolution = Evolution(self.settings, self.structure, self.CLT,
                              self.evaluation, popgenerator, population)

        while gen <= self.GA_settings.max_gen and\
                population[0].Fitness[0] > self.GA_settings.fit_tol:

            new_generation = evolution.RunGeneration(population)

            if np.mod(gen, self.GA_settings.update_cycle) == 0 or gen == 1:

                gen_performance = self.evaluation.EvaluatePopulation(new_generation)

                mean = gen_performance.avg_fitness

                if self.settings.general.print_progress:

                    print('generation {}, best fit {}, mean {}'
                          .format(gen, gen_performance.min_fitness, mean))

                    print(progress(self.GA_settings.max_gen, gen))

                self.stats.Store(gen_performance,
                                 evolution.mutation_pct,
                                 new_generation[0], gen)

            '''
            Population refresh. Generates new population every N generations
            according to GA settings. Only enforced if refresh population is true
            Keeps best individual in population
            '''

            if self.GA_settings.refresh_population and\
                    np.mod(gen, self.GA_settings.rf_freq) == 0:

                if gen != self.GA_settings.max_gen:

                    new_pop_size = self.GA_settings.pop_size - 1
                    new_pop = popgenerator.CreatePopulation(new_pop_size,
                                                            new_pop_size)

                population[self.GA_settings.elite:] = new_pop[:]

            else:

                population = new_generation[:]

            gen += 1

            self.total_gen = gen

        self.best_individual = selection.select_best(population,
                                                     self.GA_settings.fit_weights, 1)[0]

        self.final_gen = population[:]

        self.results = DataCollection(self.settings, self.structure,
                                      self.best_individual, self.stats)

        self.results.Store()

        stop = timeit.default_timer()

        print('Runtime:{} seconds'.format(round(stop - start, 2)))
