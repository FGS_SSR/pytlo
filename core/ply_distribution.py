# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:50:26 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""


import numpy as np


class ply_distribution:

    def __init__(self, settings, t_distribution):

        self.t_distribution = t_distribution

        self.panel_thk = t_distribution[1:]

        ply = settings.ply_properties
        rules = settings.design_rules

        min_thk = min([min(r) for r in self.panel_thk])

        max_thk = max([max(r) for r in self.panel_thk])

        if self.t_distribution[0] in {'thk', 'THK'}:

            self.min_ply_count = np.ceil(min_thk/ply.thickness).astype(int)

            self.max_ply_count = np.ceil(max_thk/ply.thickness).astype(int)

            self.ply_distribution = [0] * len(self.panel_thk)

            for i in range(len(self.panel_thk)):

                self.ply_distribution[i] = [np.ceil(r/ply.thickness).astype(int) for r in self.panel_thk[i]]

        else:

            self.min_ply_count = min_thk

            self.max_ply_count = max_thk

            self.ply_distribution = self.panel_thk[:]

        if rules.internal_continuity is True:

            if self.max_ply_count > (3*self.min_ply_count - 2):
                print('Target lowest thickness not in agreement with internal continuity guideline')
                self.min_ply_count = np.ceil((self.max_ply_count + 2)/3).astype(int)

        if rules.symmetry is True:

            self.N_min = np.ceil(self.min_ply_count/2).astype(int)

            self.N_max = np.ceil(self.max_ply_count/2).astype(int)

            if np.mod(self.min_ply_count, 2) != 0:
                self.min_ply_count += 1

            if np.mod(self.max_ply_count, 2) != 0:
                self.max_ply_count += 1

        else:

            self.N_min = self.min_ply_count

            self.N_max = self.max_ply_count

        self.ply_range = np.arange(self.min_ply_count, self.max_ply_count+1)
        self.set_ply_range = []
        self.set_ply_range = [R for R in self.ply_distribution if R not in self.set_ply_range]
