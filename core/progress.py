# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:32:24 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np

# progress measurement
def progress(total_size, current_state):

    pct = int((current_state/total_size)*100)

    progress = '{}%'.format(pct)

    return progress
