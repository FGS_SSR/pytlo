# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 11:16:37 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
from collections import defaultdict

from decoder import decoder

import CLT_methods


# Laminate stiffness calculations


class lamCLT:

    def __init__(self, component, settings, CLT):

        rules = settings.design_rules

        ply = settings.ply_properties

        C_ply = CLT.C_ply

        trig_dict = CLT.trig_dict

        self.compliance = {}  # not in use

        self.stiffness = {}  # not in use

        self.A = defaultdict(list)

        self.B = defaultdict(list)

        self.D = defaultdict(list)

        self.LP = defaultdict(list)

        self.LP_error = defaultdict(list)

        SStable = decoder(component)

        keys = list(SStable.keys())  # set of the patch thicknesses of te component

        for key in keys:

            laminate = CLT_methods.build_laminate(list(SStable[key]), rules, key)

            z_c = CLT_methods.z_coords(laminate, ply)

            N_ply = len(laminate)

            if settings.general.calculate_ABD is True:

                # A matrix

                self.A[key] = CLT_methods.A(laminate, ply, C_ply, N_ply)

                # B matrix

                if rules.symmetry is False:

                    self.B[key] = CLT_methods.B(laminate, z_c, C_ply, N_ply)

                else:

                    self.B[key] = 0

                # D matrix
                self.D[key] = CLT_methods.D(laminate, z_c, C_ply, N_ply)

            # LP calc

            self.LP[key] = CLT_methods.LamPam(rules, laminate, z_c,
                                              ply.thickness, N_ply, trig_dict)
