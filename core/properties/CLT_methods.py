# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 12:51:38 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

"""
import numpy as np


# CLT methods to be called in CLT class

# reduced stiffness matrix, plane stress

def Q_calc(ply):

    Q = [[(ply.Ex/(1-ply.Uxy*ply.Uyx)), (ply.Ey * ply.Uxy/(1-ply.Uxy * ply.Uyx)),0],\

         [(ply.Ey*ply.Uxy/(1-ply.Uxy*ply.Uyx)),(ply.Ey/(1-ply.Uxy * ply.Uyx)),0],\

         [0, 0, ply.Gxy]]

    return Q


# Calculates material invariants from Q

def mat_inv(Q):

    Q11 = Q[0][0]
    Q22 = Q[1][1]
    Q12 = Q[0][1]
    Q66 = Q[2][2]

    U1 = (1/8) * (3*(Q11 + Q22) + 2*Q12 + 4*Q66)
    U2 = 0.5 * (Q11 - Q22)
    U3 = (1/8) * (Q11 + Q22 - 2*Q12 - 4*Q66)
    U4 = (1/8) * (Q11 + Q22 + 6*Q12 - 4*Q66)
    U5 = (1/8) * (Q11 + Q22 - 2*Q12 + 4*Q66)

    U = [U1, U2, U3, U4, U5]

    return U


# assembles full laminate when needed (for symmetric cases)

def build_laminate(stacking_sequence, rules, key):

    if rules.symmetry is True:

        laminate = stacking_sequence + list(reversed(stacking_sequence))

    else:

        laminate = stacking_sequence[:]

    return laminate

# ply coordinates in the laminate


def z_coords(laminate, ply):

    thickness_laminate = len(laminate) * ply.thickness

    z_coords = [0] * (len(laminate)+1)

    for i in range(len(z_coords)):

        z_coords[i] = -(thickness_laminate/2) + i*ply.thickness

    return z_coords


# function used to calculate rotated ply stiffnesses

def rotate_ply(allowable_orientations, ply, Q):

    M = {}
    C_ply = {}

    for angle in allowable_orientations:

        m = np.cos(np.deg2rad(-angle))
        n = np.sin(np.deg2rad(-angle))

        M[angle] =   np.array([[m*m, n*n, 2*m*n],
                     [n*n, m*m, -2*m*n],
                     [-m*n, m*n, m*m-n*n]])

        C_ply[angle] = M[angle].dot(Q).dot(np.transpose(M[angle]))

    return C_ply


# ABD calculations


def A(laminate, ply, C_ply, n_plies):

    A = 0

    for i in range(n_plies):

        A += ply.thickness * C_ply[laminate[i]]

    return A


def B(laminate, z_coord, C_ply, n_plies):

    B = 0

    for i in range(n_plies):

        B += (1/2)*C_ply[laminate[i]]*(-pow(z_coord[i], 2)+pow(z_coord[i+1], 2))

    return B


def D(laminate, z_coord, C_ply, n_plies):

    D = 0

    for i in range(n_plies):

        D += (1/3)*C_ply[laminate[i]]*(-pow(z_coord[i], 3)+pow(z_coord[i+1], 3))

    return D


def LamPam(rules, laminate, z_coords, t_ply, n_plies, trig_dict):

    lam_thickness = n_plies * t_ply

    z1 = z_coords[:-1]
    z2 = z_coords[1:]

    inv_Nply = 1/n_plies

    V1A = 0
    V2A = 0
    V3A = 0
    V4A = 0

    for angle in laminate:
        V1A += trig_dict['cos2'][angle]
        V2A += trig_dict['sin2'][angle]
        V3A += trig_dict['cos4'][angle]
        V4A += trig_dict['sin4'][angle]


    # for VnB and VnD

    vbf = 2 / (pow(lam_thickness, 2))
    vdf = 12 / (3 * pow(lam_thickness, 3))

    V1B = 0
    V2B = 0
    V3B = 0
    V4B = 0

    V1D = 0
    V2D = 0
    V3D = 0
    V4D = 0

    for i in range(n_plies):

        ply_angle = laminate[i]

        z1_c = pow(z1[i], 3)
        z2_c = pow(z2[i], 3)

        z2c_z3c = z2_c - z1_c

        if rules.symmetry is False:
            z1_s = pow(z1[i], 2)
            z2_s = pow(z2[i], 2)

            z2s_z1s = z2_s - z1_s

            V1B += trig_dict['cos2'][ply_angle] * z2s_z1s
            V2B += trig_dict['sin2'][ply_angle] * z2s_z1s
            V3B += trig_dict['cos4'][ply_angle] * z2s_z1s
            V4B += trig_dict['sin4'][ply_angle] * z2s_z1s

        V1D += trig_dict['cos2'][ply_angle] * z2c_z3c
        V2D += trig_dict['sin2'][ply_angle] * z2c_z3c
        V3D += trig_dict['cos4'][ply_angle] * z2c_z3c
        V4D += trig_dict['sin4'][ply_angle] * z2c_z3c

    LP = [V1A * inv_Nply, V2A * inv_Nply, V3A * inv_Nply, V4A * inv_Nply,
          V1B * vbf, V2B * vbf, V3B * vbf, V4B * vbf,
          V1D * vdf, V2D * vdf, V3D * vdf, V4D * vdf]

    return LP
