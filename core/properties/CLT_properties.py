# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 16:25:50 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

File description: laminate independent properties such as ply
stiffnesses for allowable orientations. Only computed once and stored
in a dictionary.

"""
import CLT_methods
from trig_dict import trig_dict
import numpy as np


class CLT_properties:

    def __init__(self, settings):

        ply_properties = settings.ply_properties
        allowable_orientations = settings.design_rules.allowable_angles

        Q = CLT_methods.Q_calc(ply_properties)

        self.ply_stiff = Q

        self.material_invariants = CLT_methods.mat_inv(self.ply_stiff)

        self.C_ply = CLT_methods.rotate_ply(allowable_orientations, ply_properties, Q)

        self.trig_dict = trig_dict(allowable_orientations)
