# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 12:34:58 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Calculates error between LPs for a component

"""
from collections import defaultdict
import numpy as np


def LP_error_component(component, LPtarget, used_lp, panel_list):

    lp_error = defaultdict(list)
    GA_LP = component.MechProperties.LP
    num_panel = len(panel_list)

    for i in range(num_panel):

        patch_id = panel_list[i]
        Nply = component.Nstr[i]

        error = np.subtract(GA_LP[Nply], LPtarget[patch_id])

        lp_error[patch_id] = np.array([error[k] for k in used_lp])

    return lp_error
