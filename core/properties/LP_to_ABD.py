# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 18:11:46 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import CLT_methods
import numpy as np
from collections import defaultdict


def lp2polar(settings, LP, panel_thk):

    polar_coordinates = settings.general.polar_coordinates

    panel_list = [panel for panel in LP]

    panel_num = len(panel_list)

    ply_properties = settings.ply_properties

    Q = CLT_methods.Q_calc(ply_properties)

    U = CLT_methods.mat_inv(Q)

    U1, U2, U3, U4, U5 = U[0], U[1], U[2], U[3], U[4]

    A = {}
    D = {}

    Tau = {}

    Tau[0] = np.array([[U1, U4, 0], [U4, U1, 0], [0, 0, U5]])
    Tau[1] = np.array([[U2, 0, 0], [0, -U2, 0], [0, 0, 0]])
    Tau[2] = np.array([[0, 0, U2/2], [0, 0, U2/2], [U2/2, U2/2, 0]])
    Tau[3] = np.array([[U3, -U3, 0], [-U3, U3, 0], [0, 0, -U3]])
    Tau[4] = np.array([[0, 0, U3], [0, 0, -U3], [U3, -U3, 0]])

    for panel in LP:
        A[panel] = Tau[0] + LP[panel][0]*Tau[1] + LP[panel][1]*Tau[2]\
                   + LP[panel][2]*Tau[3] + LP[panel][3]*Tau[4]

        D[panel] = Tau[0] + LP[panel][8]*Tau[1] + LP[panel][9]*Tau[2]\
                   + LP[panel][10]*Tau[3] + LP[panel][11]*Tau[4]
    A_norm = {}
    D_norm = {}

    A_polar = defaultdict(dict)
    D_polar = defaultdict(dict)

    # rotation matrices
    T_matrix = {}

    for angle in polar_coordinates:

        m = np.cos(np.deg2rad(-angle))
        n = np.sin(np.deg2rad(-angle))

        T_matrix[angle] = [[m*m, n*n, 2*m*n],
                           [n*n, m*m, -2*m*n],
                           [-m*n, m*n, m*m-n*n]]

    T_transpose = {}

    for angle in T_matrix:
        T_transpose[angle] = np.transpose(T_matrix[angle])

    for i in range(panel_num):

        panel = panel_list[i]
        A_norm[panel] = A[panel]/panel_thk[i]
        D_norm[panel] = 12*D[panel]/(pow(panel_thk[i],3))
        for angle in polar_coordinates:
            A_polar[panel][angle] = T_transpose[angle].dot(A_norm[panel]).dot(T_matrix[angle])
            D_polar[panel][angle] = T_transpose[angle].dot(D_norm[panel]).dot(T_matrix[angle])

    # in- and out of plane polar stiffness

    polar_target = defaultdict(dict)

    for panel in panel_list:
        for angle in polar_coordinates:

            A11p, A12p, A13p = A_polar[panel][angle][0,0], A_polar[panel][angle][0,1], A_polar[panel][angle][0,2]
            A22p, A23p, A33p = A_polar[panel][angle][1,1], A_polar[panel][angle][1,2], A_polar[panel][angle][2,2]
            D11p, D12p, D13p = D_polar[panel][angle][0,0], D_polar[panel][angle][0,1], D_polar[panel][angle][0,2]
            D22p, D23p, D33p = D_polar[panel][angle][1,1], D_polar[panel][angle][1,2], D_polar[panel][angle][2,2]

            polar_target[panel][angle] = [A11p, A12p, A13p, A22p, A23p, A33p, D11p, D12p, D13p, D22p, D23p, D33p]

    return polar_target, A, D

