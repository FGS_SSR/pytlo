# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 12:20:38 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Creates dictionary of all sin(n*theta),cos(n*theta)
terms used in LP calculations for all allowed angles. Saves time
by not having to calculate them for each LP calculation.

"""
from collections import defaultdict
import numpy as np


def trig_dict(allowable_orientations):

    trig_dict = defaultdict(dict)

    for i in allowable_orientations:

        trig_dict['cos2'][i] = np.cos(2 * np.deg2rad(i))
        trig_dict['cos4'][i] = np.cos(4 * np.deg2rad(i))
        trig_dict['sin2'][i] = np.sin(2 * np.deg2rad(i))
        trig_dict['sin4'][i] = np.sin(4 * np.deg2rad(i))

    return trig_dict
