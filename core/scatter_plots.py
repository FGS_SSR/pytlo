# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 14:30:39 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Comparison plots between duplicate and no duplicate removal

"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker


def scatter(data, legends, title, xlabel, ylabel):

        label_size = 18
        fig1, ax1 = plt.subplots(figsize=(13, 7))

        max_x = max([max(r[0]) for r in data])
        max_y = max([max(r[1]) for r in data])

        ax1.set_xlim([0, max_x])
        ax1.set_ylim([0, max_y])

#        ax1.xaxis.set_major_locator(ticker.MultipleLocator(int(self.gen_num/20)))
#        ax1.xaxis.set_minor_locator(ticker.MultipleLocator(int(self.gen_num/100)))

        plt.grid(True)

        plt.title(r'{}'.format(title), fontsize=label_size)
        plt.xlabel(r'{}'.format(xlabel), fontsize=label_size)
        plt.ylabel(r'{}'.format(ylabel), fontsize=label_size)

        color = ['r', 'g', 'b', 'k']

        marker = ['.', '+', 'x', 'D', 'h']
        counter = 0

        for item in data:

            ax1.scatter(item[0], item[1],
                        c=color[counter], marker=marker[counter],
                        linewidth=2)
            counter += 1
        ax1.legend(legends, loc='upper right', fontsize=15)

        plt.show()