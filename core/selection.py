# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:05:12 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: multitude of selection methods based on python's
'sorted()' operator

select_best:

    fit_weights = associated fitness weights and whether goal is to
    maximize or minimize fitness (-1 is minimize, +1 maximise)

    Num_selection = How many individuals should be selected

select_random:

     selects Num_selection individuals randomly from a list of individuals

select_parents:

    Selects 2 individuals from a given list tournament style.

"""

from operator import attrgetter

import random


def select_best(individuals, fit_weights, Num_selection):

    if fit_weights[0] < 0:

        selected = sorted(individuals, key=attrgetter('Fitness'),
                          reverse=False)[:Num_selection]

    else:
            selected = sorted(individuals, key=attrgetter('Fitness'),
                              reverse=True)[:Num_selection]

    return selected


def select_random(individuals, Num_selection):

    return [random.choice(individuals) for i in range(Num_selection)]


def select_parents(individuals, tournament_size, fit_weights):

    candidates = select_random(individuals, tournament_size)

    parents = select_best(candidates, fit_weights, 2)

    return parents
