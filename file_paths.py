# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 15:39:23 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: file path setup

"""

import sys

sys.path.append('./core')
sys.path.append('./core/check')
sys.path.append('./core/EA')
sys.path.append('./core/initial_pop')
sys.path.append('./core/properties')
sys.path.append('./core/data_mgt')

sys.path.append('./fitness_functions')

sys.path.append('./project_data')
