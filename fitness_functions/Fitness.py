"""
Created on 28/11/2018 at 13:00

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Fitness class containing all fitness methods.
New fitness functions should be added under 'Add fitness methods to class'

"""
import fitness_functions.LP_RMSE as LP_RMSE


class Fitness:
    def __init__(self, settings, structure, CLT):
        self.settings = settings
        self.structure = structure
        self.CLT = CLT

    # Add fitness methods to class:

    LP_RMSE = LP_RMSE.LP_RMSE
