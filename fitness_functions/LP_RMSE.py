# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 14:32:17 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

File description: LP RMSE - Calculates RMSE error for each panel,
 returns an average over whole structure
"""
from rms_LP_calc import rms_LP


def LP_RMSE(self, component):

    component_id = component.ID
    structure = self.structure.components[component_id]
    panel_list = structure.panel_list

    LP_weights = structure.objective.relevant_LP_weights

    N_panels = structure.num_panel

    fitness = 0

    for panel in panel_list:

        error = component.MechProperties.LP_error[panel]

        fitness += rms_LP(error, LP_weights, N_panels)

    return fitness
