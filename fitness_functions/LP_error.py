# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 12:34:58 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Calculates error between LPs for a single patch

"""


def LP_error(GA_LP, target_LP):

    lp_error = [0]*12

    for i in range(12):
        lp_error[i] = GA_LP[i] - target_LP[i]

    return lp_error
