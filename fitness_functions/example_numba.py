# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 10:58:59 2019

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Example code using the numba library

mxn_array = a numpy array of m x n dimensions
n_array = a numpy array of 1 x n dimension

each row in the mxn_array is multiplied with n_array and then summed up.

The resulting value is placed in the sum_array variable.

"""

import numpy as np
from numba import jit


@jit(nopython=True)
def example_numba(mxn_array, n_array):

    num_elements = len(n_array)

    sum_array = np.zeros((num_elements))

    for i in range(num_elements):
        LP_mult = np.multiply(mxn_array[i], n_array)
        sum_array[i] = np.sum(LP_mult)

    return sum_array