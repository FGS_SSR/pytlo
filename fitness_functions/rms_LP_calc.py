# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 15:51:47 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: calculates rms of LP error using numba for better performance

"""
import numpy as np
from numba import jit


@jit(nopython=True)
def rms_LP(error, LP_weights, N_panels):

    weighed_error = np.multiply(error, LP_weights)

    rms = (1/N_panels)*np.sqrt(np.square(weighed_error).mean())

    return rms
