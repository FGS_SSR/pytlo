# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 10:16:08 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com

file description: User defined problem setup where fitness type and target values are filled

"""
from project_data.read_target import read_target
from core.ply_distribution import ply_distribution
import numpy as np


class Objective:
    def __init__(self, settings, panel_list, component):

        # target LP/THK file
        design_var = read_target('project_data/ref/design_var.csv')

        '''
        Input thickness list or ply number list

        inputs are as follows:

            thk_distribution = ['ply', [upper bound, lower bound], ...] where
            an upper and lower bound for each panel is defined:

            example:

            thk_distribution = ['ply', [10, 8], [8,6],[6,4],[4,4]]

            'ply' can be replaced with 'thk' if panel thicknesses are given in [mm]
            instead of ply numbers
        '''

        self.thk_distribution = ['ply']

        for patch in panel_list:

            self.thk_distribution += [design_var['THK'][patch]]

        # Contains ply number and range per panel
        self.ply_distribution = ply_distribution(settings, self.thk_distribution)

        # LP obtained from continuous optimization

        '''
        LP input in the form of {Panel 1: [LP],Panel 2: [LP],Panel N: [LP]}

        LP in the form of [Panel N LP] = [V1A, V2A, V3A, V4A, V1B, V2B, V3B, V4B, V1D, V2D, V3D, V4D]

        '''

        self.LP_target = {}

        for patch in panel_list:

            self.LP_target[patch] = design_var['LP'][patch]

        # weights

        self.LP_weights = np.array([1, 1, 1, 1,
                                    0, 0, 0, 0,
                                    1, 1, 1, 1])

        self.panel_weights = [1]*len(panel_list)

        # Whether varying panel importance is used in fitness calculation
        self.enforce_panel_weights = False

        # indexes of LP with weights != 0
        self.relevant_LP = [i for i, x in enumerate(self.LP_weights) if x != 0]
        self.relevant_LP_weights = np.array([self.LP_weights[k] for k in self.relevant_LP])

        '''
        Extra attributes can be added by user depending on his project.
        Sensitivities for response approximation, weight, displacement, etc. 
        can all be considered so long as the user creates an objective
        function that uses them. 
        
        Given the variability in data storage methods and project specific
        information, this feature is left to the user to implement.
        '''