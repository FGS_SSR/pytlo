# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 11:35:12 2019

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Read target design variables from file

"""
import csv
from collections import defaultdict


def read_target(file_loc):

    design_var = defaultdict(dict)

    with open(file_loc) as datafile:

        readCSV = csv.reader(datafile, delimiter=',')

        next(readCSV)

        for row in readCSV:

            patch_id = int(row[0])

            design_var['LP'][patch_id] = [float(val) for val in row[1:13]]
            design_var['THK'][patch_id] = [int(row[13]), int(row[14])]

    return design_var