# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 14:29:54 2019

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Main interface from where the optimiser is run. For more information,
refer to the documentation. Multiple "jobs" can be created if multiprocessing is desired

"""

import file_paths
from core.main import main

from core.data_mgt.multi_run_collection import multi_run_collection
from settings import Settings
from structure import Structure

num_runs = 1    # desired number of runs

settings = Settings()
structure = Structure(settings)

total_runs = 0  # counter

run_data = {}

while total_runs < num_runs:

    opt, plots = main(settings, structure)

    multi_run_collection(settings, opt)  # collect fitness of each run

    run_data[settings.data_settings.GA_counter] = [opt, plots]

    settings.data_settings.GA_counter += 1

    total_runs += 1
