# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 13:30:10 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: File where all settings are defined. Includes population
generation settings, GA settings and problem setup. For more information on 
the different fields, refer to the manual.

"""
import numpy as np


class PlyProperties:
    def __init__(self):
        self.Ex         = 177000   # MPa
        self.Ey         = 10800    # MPa
        self.Uxy        = 0.27
        self.Uyx        = (self.Ey/self.Ex)*self.Uxy
        self.Gxy        = 7600   # MPa
        self.density    = 1  # Kg/m3
        self.thickness  = 0.2 # mm


class ManufacturingConstraints:
    def __init__(self):

        step         = 15
        self.allowable_angles   = np.arange(-90 + step, 90 + step, step).tolist()

        self.symmetry            = True
        self.balance             = False
        self.ten_pct             = False
        self.pct_breakdown       = [0.1, 0.1, 0.1, 0.1] # [0, 90, 45, -45]
        self.damtol              = False
        self.ply_contiguity      = False
        self.angle_diff          = False
        self.covering            = 'Outer'
        self.ply_drop            = 99
        self.internal_continuity = False
        self.enforce_thk         = True

        if self.internal_continuity is True:
            self.max_continuity = 4     # average number of plies between continuous (rank 0) plies

        if self.ply_contiguity is True:
            self.contiguity_n = 3       # maximum number of adjacent plies of same angle

        if self.angle_diff is True:
            self.misorientation = 45    # maximum angle difference between adjacent plies

        else:
            self.misorientation = 180

        if self.damtol is True:
            # Defines allowable angle range for outer plies to respect damage tolerance requirements
            self.damtol_upper_lim = 45
            self.damtol_lower_lim = 45

        if self.ten_pct is True:
            self.pct_zero = self.pct_breakdown[0]
            self.pct_ninety = self.pct_breakdown[1]
            self.pct_ff = self.pct_breakdown[2]
            self.pct_nff = self.pct_breakdown[3]

        # how many plies should be used as cover from thinnest laminate
        self.cover_thk = 0


class GaSettings:
    def __init__(self):
        self.elitism            = 0.01  # percentage of elite population
        self.initial_pop        = 160   # Size of generated pop from which best 'pop_size' are selected
        self.pop_size           = 80
        self.cx_fraction        = 0.75  # mating fraction
        self.mut_pb             = 0.25  # probability of mutation for each individual and genotype
        self.max_gen            = 2000  # maximum number of generations
        self.rf_freq            = 150   # Frequency of pop_refresh (if refresh_population is True)
        self.fit_tol            = 0.001 # fitness tolerance
        self.update_cycle       = 15    # how often the progress should be printed
        self.fit_weights        = [-1,] # fitness weight(s) if negative: minimisation
        self.delete_duplicates  = False # discard duplicate individuals during crossover.
        self.refresh_population = False # creates new individuals from scratch and adds it to mating pool

        self.elite              = np.ceil(self.elitism * self.pop_size).astype(int)


class General:
    def __init__(self):
        # currently available fit types ['LP_RMSE']
        self.calculate_ABD = False
        self.fitness_type = 'LP_RMSE'
        self.print_progress = True


class DataSettings:
    def __init__(self):

        self.timestamp_dir   = False            # if True, result folders are created with YY/MM/DD parent folder and HH/MM/SS for each run
        self.batch_id        = 'HS LPrms'   # results folder where run(s) are stored.
        self.run_id          = 'GA'           # run folder name as self.run_id + self.GA_counter sequence.
        self.GA_counter      = 1                # Run counter


class Settings:
    def __init__(self):

        self.ply_properties = PlyProperties()
        self.design_rules   = ManufacturingConstraints()
        self.GA_settings    = GaSettings()
        self.general        = General()
        self.data_settings  = DataSettings()