# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 16:09:23 2018

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Here the structure to be optimised is defined 
in terms of panel connectivity, target objective, etc.

"""
from collections import defaultdict
from core.Component import Component


class Structure:

    def __init__(self, settings):

        connectivity = True

        self.components = defaultdict(list)

        self.panel_list = defaultdict(list)

        self.panel_list['root'] = [1, 2, 9, 10, 11, 12]
        self.panel_list['UP'] = [3, 4, 5, 6, 7, 8]
        self.panel_list['LP'] = [13, 14, 15, 16, 17, 18]

        self.full_panel_list = []

        for item in self.panel_list:
            self.full_panel_list += self.panel_list[item]

        self.total_panel_count = len(self.full_panel_list)

        # define which components should weigh more on fitness
        self.component_weights = {'root': 1, 'UP': 1, 'LP': 1}

        if connectivity is True:

            # only needs to be filled in if connectivity needed
            # format: {patch_id: [list_of_neighbour_panels]}

            neighbours = {1: [2, 9],
                          2: [1, 10],
                          3: [4, 6],
                          4: [3, 5, 7],
                          5: [4, 8],
                          6: [3, 7],
                          7: [4, 6, 8],
                          8: [5, 7],
                          9: [1, 10, 11],
                          10: [2, 9, 12],
                          11: [9, 12],
                          12: [10, 11],
                          13: [14, 16],
                          14: [13, 15, 17],
                          15: [14, 18],
                          16: [13, 17],
                          17: [14, 16, 18],
                          18: [15, 17]}

            # creates target component with objective file

            for component in self.panel_list:
                self.components[component] = Component(settings, component,
                                                       self.panel_list[component],
                                                       neighbours=neighbours)
        else:

            for component in self.panel_list:
                self.components[component] = Component(settings, component,
                                                       self.panel_list[component])
